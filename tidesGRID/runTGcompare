#!/bin/bash
#
# Program to have data ready for comparison to tide gauges. 
# The scripts callCombination_GRID and then prepareFiles are called.
# in each folder of each script, a new subfolder is created with the 
# corresponding results.
# normally, the name of the folder indicates the test that we are running or the region
# of interest analysed. For example, the folder "testIDXem330" means that we are running 
# a test for a cap size of 330 km, including extended missions, with some coastal
# indexing IDX.

# Give the name of output folder
dst=EOTsla34_avgSla_VCE_varCap_lat_nSea

# Select the area of interest
reg=-12/4/49/61
# global reg=-180/180/-90/90
# yellow sea reg=120/130/22.5/41
# north sea reg=-12/4/49/61
# baltic sea reg=5/31/49/66
# indonesia reg=90/150/-15/15

# re-write the region limits as single variables (needed to use awk)
# VERY IMPORTANT: NEVER GIVE EXACTLY 0, BUT ALWAYS USE E.G. 0.001 OR -0.001, 
# SO THAT WE KNOW WHICH LONGITUDE DOMAIN WE ARE USING (if 0 to 360 or -180 to 180)
lonmin=$(echo $reg | cut -f1 -d/)
lonmax=$(echo $reg | cut -f2 -d/)
latmin=$(echo $reg | cut -f3 -d/)
latmax=$(echo $reg | cut -f4 -d/)


# Select the constituents to be processed 
# (note that some tide gauges in the baltic do not contain K2 and P1, therefore remove these
# two constituents from the computation)
declare -a arr=(M2 N2 S2 K2 K1 O1 P1 Q1 M4 MM MF SA SSA 2N2 S1)
#  M2 N2 S2 K2 K1 O1 P1 Q1     M4 MM MF SA SSA 2N2 S1
# Select grid spacing
spacing=7.5m

# Declare path of tidal residuals to be used
pth=/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_avgSla_VCE_varCap_lat_nSea

# Write the suffix of tidal residual files (e.g. M2_XX, XX is the suffix)
sfx=9999

# --------------------------------------------------------------------------------------

# # 1. callCombination_GRID
cd /DGFI8/H/work_gaia/tidesGRID/combine
# 
# create destination folder if doesn't exist
if [ ! -d $dst ]; then
  mkdir $dst
fi

# # call script
if [ $sfx -eq 9999 ]; then
./callCombination_GRID_NOsfx<< EOF   
${arr[@]}
$reg
$spacing
$pth
$dst
EOF
else
./callCombination_GRID<< EOF   
${arr[@]}
$reg
$spacing
$pth
$dst
$sfx
EOF
fi

# --------------------------------------------------------------------------------------

# 2. prepareFiles
# cd /DGFI8/H/work_gaia/tidesGRID/TGcomparison
# 
# # create destination folder if doesn't exist
# if [ ! -d $dst ]; then
#   mkdir $dst
# fi
# 
# # call script
# ./prepareFiles<< EOF   
# $lonmin
# $lonmax
# $latmin
# $latmax
# ${arr[@]}
# $dst
# $reg
# EOF

# --------------------------------------------------------------------------------------



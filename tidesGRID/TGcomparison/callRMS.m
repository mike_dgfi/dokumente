% This program is stored in folder /DGFI8/H/work_gaia/tidesGRID/TGcomparison/

% Get Root mean squares of tg VS models by calling function RMS
clear all
close all

% if RMS function can't be found, please type:
addpath('/DGFI93/home/piccioni/MATLAB')

% API for plotting google Earth maps with loose limit in number of plots
API = 'AIzaSyCDPJKvX73RA0HBqRd1PIldhszqReBs0dg';

d2r=pi/180;

% number of constituents to be analysed
% ntides=9;

% define tidal constants 
% IMPORTANT: they must have the same order as in the vector declared
% in the TGcomparison script!
const = {'M2'; 'N2'; 'S2'; 'K2'; 'K1'; 'O1'; 'P1'; 'Q1'};  %; 'M4'
% IMPORTANT: 'K2' and 'P1' do not exist for some of Ray's TG data. In the
% Baltic Sea, for example.  
%  

% w = d2r*[28.98410424 28.43972953 30 15.04106864 13.94303560 13.39866089 57.96820848];
% t = [linspace(0,12.4206,20); linspace(0,12.6584,20); linspace(0,12,20); ...
%      linspace(0,23.9344,20); linspace(0,25.8194,20); ...
%      linspace(0,26.8684,20); linspace(0,6.2103,20)];
% import amplitude and phase of in-situ data
pathData = '/DGFI8/H/work_gaia/tidesGRID/TGcomparison/EOTsla34_avgSla_VCE_selCapLatVar_nSea';
tg   = dlmread([pathData '/tgALL.txt']);  

% save the TG coordinates in the right order to be used for e.g. number of
% observations for the missions 
dlmwrite('tgCoord',tg(:,1:2),'delimiter',' ','precision','%.4f')

% tidal results from grids interpolated on TG locations
eot19 = dlmread([pathData '/eot18ALL.txt']);
eot11 = dlmread([pathData '/eot11ALL.txt']);
% [imin,jmin]= find(eot11<0);
% eot11(imin,jmin)=eot11(imin,jmin)+360;

fes14 = dlmread([pathData '/fes14ALL.txt']);
tpxo8 = dlmread([pathData '/tpxo8ALL.txt']);

dtu16 = dlmread([pathData '/dtu16ALL.txt']);
got48 = dlmread([pathData '/got48ALL.txt']);
% fes04 = dlmread([pathData '/fes04ALL.txt']);
fes12 = dlmread([pathData '/fes12ALL.txt']);
fes12(:,3:2:end)=fes12(:,3:2:end).*100;

% number of observations at each node for ESA and NASA missions
% esa = dlmread('/DGFI93/home/piccioni/Dokumente/tidesGRID/results/esaObsTG.txt');
% nasa = dlmread('/DGFI93/home/piccioni/Dokumente/tidesGRID/results/nasaObsTG.txt');

% remove all NaNs (or 0 values in case of TPXO8)
% N.B. These right below are the general conditions to remove only TGs with
% NaNs. In order to remove also the TGs assimilated in FES, please refer to
% the following special setups (for Baltic sea, Indonesia, Yellow Sea, and
% so on. The user may only comment the following lines and de-comment the
% special setups here below. 
% Because in many areas the TGs avoided are many, I used an awk script to
% write faster the conditions in matlab:
% awk '{print "(tg(:,1)~=",$1," & tg(:,2)~=",$2,") & ..."}' tmp
% ix = find(~isnan(eot19(:,3)) & ~isnan(fes14(:,3)) & ... 
%           ~isnan(eot11(:,3)) & ~isnan(dtu16(:,3)) & ...
%          ~isnan(got48(:,3)) ); 



%    
% ----------
% setup for baltic sea to avoid tgs assimilated in FES14. Do not include in 
% the analysis GOT4.8 and TPXO8 that have NaNs for many stations
% ix = find(~isnan(eot19(:,3)) & ~isnan(fes14(:,3)) & ... 
%           ~isnan(eot11(:,3)) & tg(:,1) > 12.3 & ...
%           tg(:,1)~=18.0817 & tg(:,2)~=59.325 & ...
%           tg(:,1)~=18.0817 & tg(:,2)~=59.3242); 


% ----------
% setup for north sea, to avoid also tgs assimilated in FES2014
ix = find(~isnan(eot19(:,3)) & ~isnan(fes14(:,3)) & ... 
          ~isnan(eot11(:,10))  & tg(:,2)<60.15 & ...
          ~isnan(eot11(:,3))  & ~isnan(dtu16(:,3)) & ...
          ~isnan(got48(:,3)) & ~isnan(tpxo8(:,3)) &...
           (tg(:,1)~=0.7431 & tg(:,2)~=53.3078) & ...
           (tg(:,1)~=352.667 & tg(:,2)~=55.3667)  & ...
           (tg(:,1)~=352.67 & tg(:,2)~=55.3717 ) & ...
           (tg(:,1)~=350.817 & tg(:,2)~=51.5333) & ...
           (tg(:,1)~=358.86 & tg(:,2)~=60.1550 ) & ...
           (tg(:,1)~=358.867 & tg(:,2)~=60.1500 ) & ...
           (tg(:,1)~=358.86 & tg(:,2)~=60.1540) & ...
           (tg(:,1)~=357.285 & tg(:,2)~=51.5109) & ...
           (tg(:,1)~=357.013 & tg(:,2)~=51.5500 ) & ...
           (tg(:,1)~=357.272 & tg(:,2)~=51.5000) & ...
           (tg(:,1)~=357.552 & tg(:,2)~=50.6085) & ...
           (tg(:,1)~=353.612 & tg(:,2)~=58.2083 ) & ...
           (tg(:,1)~=353.611 & tg(:,2)~=58.2071) & ...
           (tg(:,1)~=353.612 & tg(:,2)~=58.2079) & ...
           (tg(:,1)~=354.458 & tg(:,2)~=50.1017) & ...
           (tg(:,1)~=354.458 & tg(:,2)~=50.1024) & ...
           (tg(:,1)~=-6.6569999 & tg(:,2)~=55.2068)  & ...
           (tg(:,1)~=354.457 & tg(:,2)~=50.1030));
%           
% ----------
% setup for indonesia, to avoid also tgs assimilated in FES2014
% ix = find(~isnan(eot19(:,3)) & ~isnan(fes14(:,3)) & ...
%     ~isnan(eot11(:,3)) & ~isnan(dtu16(:,3)) &  ~isnan(got48(:,3)) &...
%      tg(:,2)>=-10 & tpxo8(:,3)~=0 &...
%     (tg(:,1)~= 125.1933  & tg(:,2)~= 1.4400 ) & ...
%     (tg(:,1)~= 141.9167  & tg(:,2)~= -10.6000 ) & ...
%     (tg(:,1)~= 123.7500  & tg(:,2)~= 13.1500 ) & ...
%     (tg(:,1)~= 105.6667  & tg(:,2)~= -10.4167 ) & ...
%     (tg(:,1)~= 109.0167  & tg(:,2)~= -7.7517 ) & ...
%     (tg(:,1)~= 109.2550  & tg(:,2)~= 13.7750 ) & ...
%     (tg(:,1)~= 96.8833  & tg(:,2)~= -12.1167 ) & ...
%     (tg(:,1)~= 96.9000  & tg(:,2)~= -12.1167 ) & ...
%     (tg(:,1)~= 130.8500  & tg(:,2)~= -12.4667 ) & ...
%     (tg(:,1)~= 125.6333  & tg(:,2)~= 7.0833 ) & ...
%     (tg(:,1)~= 144.6500  & tg(:,2)~= 13.4333 ) & ...
%     (tg(:,1)~= 99.8167  & tg(:,2)~= 11.7950 ) & ...
%     (tg(:,1)~= 99.7650  & tg(:,2)~= 6.4317 ) & ...
%     (tg(:,1)~= 98.4250  & tg(:,2)~= 7.8317 ) & ...
%     (tg(:,1)~= 98.7667  & tg(:,2)~= 1.7500 ) & ...
%     (tg(:,1)~= 98.7667  & tg(:,2)~= 1.7500 ) & ...
%     (tg(:,1)~= 147.3667  & tg(:,2)~= -2.0333 ) & ...
%     (tg(:,1)~= 147.2717  & tg(:,2)~= -2.0133 ) & ...
%     (tg(:,1)~= 120.2100  & tg(:,2)~= 13.8200 ) & ...
%     (tg(:,1)~= 120.9667  & tg(:,2)~= 14.5833 ) & ...
%     (tg(:,1)~= 120.9683  & tg(:,2)~= 14.5850 ) & ...
%     (tg(:,1)~= 100.3750  & tg(:,2)~= -0.9967 ) & ...
%     (tg(:,1)~= 134.4633  & tg(:,2)~= 7.3300 ) & ...
%     (tg(:,1)~= 134.4833  & tg(:,2)~= 7.3333 ) & ...
%     (tg(:,1)~= 134.4633  & tg(:,2)~= 7.3300 ) & ...
%     (tg(:,1)~= 144.7967  & tg(:,2)~= 13.4283 ) & ...
%     (tg(:,1)~= 111.7300  & tg(:,2)~= -8.2800 ) & ...
%     (tg(:,1)~= 109.2550  & tg(:,2)~= 13.7750 ) & ...
%     (tg(:,1)~= 131.2900  & tg(:,2)~= -7.9817 ) & ...
%     (tg(:,1)~= 107.0717  & tg(:,2)~= 10.3400 ) & ...
%     (tg(:,1)~= 138.1333  & tg(:,2)~= 9.5167 ) & ...
%     (tg(:,1)~= 138.1283  & tg(:,2)~= 9.5083 ) ...
%     );
% 
% ----------
% setup for yellow sea, to avoid also tgs assimilated in FES2014
% ix = find(~isnan(eot19(:,3)) & ~isnan(fes14(:,3)) & ...
%     tg(:,2)>=30 &...
%     (tg(:,1)~= 129.8417  & tg(:,2)~= 29.8417 ) & ...
%     (tg(:,1)~= 129.8667  & tg(:,2)~= 32.7333 ) & ...
%     (tg(:,1)~= 129.8667  & tg(:,2)~= 32.7333 ) & ...
%     (tg(:,1)~= 129.8667  & tg(:,2)~= 32.7333 ) & ...
%     (tg(:,1)~= 129.4950  & tg(:,2)~= 28.3817 ) & ...
%     (tg(:,1)~= 124.1500  & tg(:,2)~= 24.3333 ) & ...
%     (tg(:,1)~= 124.1500  & tg(:,2)~= 24.3333 ) & ...
%     (tg(:,1)~= 124.1667  & tg(:,2)~= 24.3333 ) & ...
%     (tg(:,1)~= 127.6667  & tg(:,2)~= 26.2167 ) & ...
%     (tg(:,1)~= 127.6667  & tg(:,2)~= 26.2167 ) & ...
%     (tg(:,1)~= 127.6667  & tg(:,2)~= 26.2000 ) );
% 

% ------------------------
% setup for the whole world (condition saved to external file called
% tgfes.m
% you may want to change the names of the other models (go to tgfes.m and 
% change the first lines).

% tgfes
% modify longitude range to plot with google Earth



eot19 = eot19(ix,:);
eot11 = eot11(ix,:);
fes14 = fes14(ix,:);
tpxo8 = tpxo8(ix,:);
dtu16 = dtu16(ix,:);
got48 = got48(ix,:);
tg    = tg(ix,:);
fes12 = fes12(ix,:);
% fes04 = fes04(ix,:);
% esa   = esa(ix,:);
% nasa  = nasa(ix,:);

i360 = find(tg(:,1)>180);
tg(i360,1)=tg(i360,1)-360;
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
signal=sqrt(mean(((tg(:,3:2:end-1)).^2),1)./2); % like Stammer et al.2014
% 
for i = 1:length(tg(:,1))
    
    count = 1;
    
    % compute the RMS values for the selected nodes and store them
    for j = 3:2:length(tg(1,:))-1
        
        rmsEOT19(i,count) = RMS(tg(i,j),tg(i,j+1)*d2r,eot19(i,j),eot19(i,j+1)*d2r);    
        rmsEOT11(i,count) = RMS(tg(i,j),tg(i,j+1)*d2r,eot11(i,j),eot11(i,j+1)*d2r);
        rmsFES14(i,count) = RMS(tg(i,j),tg(i,j+1)*d2r,fes14(i,j),fes14(i,j+1)*d2r);
        rmsTPXO8(i,count) = RMS(tg(i,j),tg(i,j+1)*d2r,tpxo8(i,j),tpxo8(i,j+1)*d2r);
        rmsDTU16(i,count) = RMS(tg(i,j),tg(i,j+1)*d2r,dtu16(i,j),dtu16(i,j+1)*d2r);
        rmsGOT48(i,count) = RMS(tg(i,j),tg(i,j+1)*d2r,got48(i,j),got48(i,j+1)*d2r);  
%         rmsFES12(i,count) = RMS(tg(i,j),tg(i,j+1)*d2r,fes12(i,j),fes12(i,j+1)*d2r);
%         rmsFES04(i,count) = RMS(tg(i,j),tg(i,j+1)*d2r,fes04(i,j),fes04(i,j+1)*d2r);
        gmadFES14(count) =  gmad(tg(:,j).*cos(tg(:,j+1)*d2r),tg(:,j).*sin(tg(:,j+1)*d2r), ...
            fes14(:,j).*cos(fes14(:,j+1)*d2r),fes14(:,j).*sin(fes14(:,j+1)*d2r));
        
        gmadEOT11(count) =  gmad(tg(:,j).*cos(tg(:,j+1)*d2r),tg(:,j).*sin(tg(:,j+1)*d2r), ...
            eot11(:,j).*cos(eot11(:,j+1)*d2r),eot11(:,j).*sin(eot11(:,j+1)*d2r));
        
        gmadEOT19(count) =  gmad(tg(:,j).*cos(tg(:,j+1)*d2r),tg(:,j).*sin(tg(:,j+1)*d2r), ...
            eot19(:,j).*cos(eot19(:,j+1)*d2r),eot19(:,j).*sin(eot19(:,j+1)*d2r));
        
        gmadTPXO8(count) =  gmad(tg(:,j).*cos(tg(:,j+1)*d2r),tg(:,j).*sin(tg(:,j+1)*d2r), ...
            tpxo8(:,j).*cos(tpxo8(:,j+1)*d2r),tpxo8(:,j).*sin(tpxo8(:,j+1)*d2r));
        
        gmadDTU16(count) =  gmad(tg(:,j).*cos(tg(:,j+1)*d2r),tg(:,j).*sin(tg(:,j+1)*d2r), ...
            dtu16(:,j).*cos(dtu16(:,j+1)*d2r),dtu16(:,j).*sin(dtu16(:,j+1)*d2r));
        
        gmadGOT48(count) =  gmad(tg(:,j).*cos(tg(:,j+1)*d2r),tg(:,j).*sin(tg(:,j+1)*d2r), ...
            got48(:,j).*cos(got48(:,j+1)*d2r),got48(:,j).*sin(got48(:,j+1)*d2r));
               

        count = count + 1;
        
    end
      
end
% 
% 
% iii = find(rmsFES14(:,1)>20);
% 
% rmsEOT19(iii,:) = [];
% rmsEOT11(iii,:) = [];
% rmsFES14(iii,:) = [];
% rmsTPXO8(iii,:) = [];
% rmsDTU16(iii,:) = [];
% rmsGOT48(iii,:) = [];
% tg(iii,:)       = [];
% 
av_rmsEOT19=sqrt(mean(rmsEOT19.^2)./2);
av_rmsEOT11=sqrt(mean(rmsEOT11.^2)./2);
av_rmsFES14=sqrt(mean(rmsFES14.^2)./2);
av_rmsTPXO8=sqrt(mean(rmsTPXO8.^2)./2);
av_rmsDTU16=sqrt(mean(rmsDTU16.^2)./2);
av_rmsGOT48=sqrt(mean(rmsGOT48.^2)./2);
% av_rmsFES12=sqrt(mean(rmsFES12.^2)./2);
% av_rmsFES04=sqrt(mean(rmsFES04.^2)./2);


% figure
% plot(t(count,:),s1,'b')
% hold on
% plot(t(count,:),s2,'r')
% grid on
% 
% figure
% plot(t(count,:),s1-s2,'g')
% hold on
% plot(t(count,:),test,'m')


% remove tide gauges which have high RMS with all models, e.g. above 30 cm
% iii = 37;


% esa(iii,:)      = [];
% nasa(iii,:)     = [];
% 
% 
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Make tables for averaged rms and rss results

% print average RSS and average RMS
rssSt14_avg = [sqrt(sum(av_rmsEOT11.^2)) sqrt(sum(av_rmsEOT19.^2)) ...
           sqrt(sum(av_rmsFES14.^2)) sqrt(sum(av_rmsTPXO8.^2)) ...
           sqrt(sum(av_rmsDTU16.^2)) sqrt(sum(av_rmsGOT48.^2))  ...
           ];
       
disp('Average RMS error')
T = table([av_rmsEOT11'; rssSt14_avg(1)], [av_rmsEOT19'; rssSt14_avg(2)], ...
          [av_rmsFES14'; rssSt14_avg(3)], [av_rmsTPXO8'; rssSt14_avg(4)], ...
          [av_rmsDTU16'; rssSt14_avg(5)], [av_rmsGOT48'; rssSt14_avg(6)], ...
         'RowNames',vertcat(const,'RSS'));

T.Properties.VariableNames ={'EOT11','EOT19p','FES14','TPXO8','DTU16', ...
                             'GOT4v8'};
disp(T)
disp(' ')
disp(' ')
% mean(rmsEOT19)'
% av_rmsFES12',av_rmsFES04',
% , 'RMS_FES12', 'RMS_FES04'
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% make table with median absolute differences, rms and rss
disp('Median of absolute differences')
% mean(rssEOT19),'EOT19',

rssSt14_mad = [sqrt(sum(gmadEOT11.^2)) sqrt(sum(gmadEOT19.^2)) ...
           sqrt(sum(gmadFES14.^2)) sqrt(sum(gmadTPXO8.^2)) ...
           sqrt(sum(gmadDTU16.^2)) sqrt(sum(gmadGOT48.^2))  ...
           ];

T = table([gmadEOT11'; rssSt14_mad(1)], [gmadEOT19'; rssSt14_mad(2)], ...
          [gmadFES14'; rssSt14_mad(3)], [gmadTPXO8'; rssSt14_mad(4)], ...
          [gmadDTU16'; rssSt14_mad(5)],[gmadGOT48'; rssSt14_mad(6)],  ...
          'RowNames',vertcat(const,'RSS'));
T.Properties.VariableNames ={'EOT11','EOT19p','FES14','TPXO8','DTU16', ...
                             'GOT4v8'};
disp(T)



% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% compute RSS for each tide gauge, based on averaged rms
for i=1:length(tg(:,1))
    
    rssEOT19(i) = sqrt(nansum(rmsEOT19(i,:)));
    rssEOT11(i) = sqrt(nansum(rmsEOT11(i,:)));
    rssFES14(i) = sqrt(nansum(rmsFES14(i,:)));
    rssTPXO8(i) = sqrt(nansum(rmsTPXO8(i,:)));
    rssDTU16(i) = sqrt(nansum(rmsDTU16(i,:)));
    rssGOT48(i) = sqrt(nansum(rmsGOT48(i,:)));
%     rssHAM11(i) = sqrt(nansum(rmsHAM11(i,:)));
%     rssOSU12(i) = sqrt(nansum(rmsOSU12(i,:)));
end



% 
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   PLOTS   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
% Plot RSS differences geographically: EOT19 - EOT11
% ( RSS(EOT11)-RSS(EOT19)  ) / RSS(EOT11)
% green = improvement with EOT19, red = worsening with EOT19
figure(1)
scatter(tg(:,1),tg(:,2),70,(rssEOT11-rssEOT19)./rssEOT11.*100,'fill')
hold on
polarmap_inverted
h=colorbar;
title(h,'[%]')
set(gca,'FontSize',15)
caxis([-30 30])
hold on
title('Improvement with EOT19 wrt EOT11')
plot_google_map('APIKey',API,'MapType','satellite')

dd = [tg(:,1) tg(:,2) ((rssEOT11-rssEOT19)./rssEOT11.*100)'];
dlmwrite('diffEOT_NSea_capMaskVarLat',dd,'delimiter',' ','precision','%.4f')

% mm18 = [tg(:,1) tg(:,2) rmsEOT19(:,1)];
% mm11 = [tg(:,1) tg(:,2) rmsEOT11(:,1)];
% dlmwrite('rmsM2_eot19',mm18,'delimiter',' ','precision','%.4f')
% dlmwrite('rmsM2_eot11',mm11,'delimiter',' ','precision','%.4f')

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
% Plot RMS differences geographically: EOT19 - EOT11  (there are some
% problems with K1 constituent)
% ( RMS(EOT11)-RMS(EOT19)  ) / RMS(EOT11)
% green = improvement with EOT19, red = worsening with EOT19
figure(10)
scatter(tg(:,1),tg(:,2),70,(rmsEOT11(:,5)-rmsEOT19(:,5))./rmsEOT11(:,5).*100,'fill')
hold on
polarmap_inverted
h=colorbar;
title(h,'[%]')
set(gca,'FontSize',15)
caxis([-30 30])
hold on
title('Improvement with EOT19 wrt EOT11 for K1 constituent')
plot_google_map('APIKey',API,'MapType','satellite')

% dd = [tg(:,1) tg(:,2) ((rmsEOT11(:,5)-rmsEOT19(:,5))./rmsEOT11(:,5).*100)];
% dlmwrite('diffEOT_nseaK1',dd,'delimiter',' ','precision','%.4f')

mm18 = [tg(:,1) tg(:,2) rmsEOT19(:,1)];
mm11 = [tg(:,1) tg(:,2) rmsEOT11(:,1)];
dlmwrite('rmsM2_eot19',mm18,'delimiter',' ','precision','%.4f')
dlmwrite('rmsM2_eot11',mm11,'delimiter',' ','precision','%.4f')


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Plot RSS improvement in % geographically: 
% ( RSS(FES2014) - RSS(EOT19) ) / RSS(FES2014)
% green = improvement with EOT19, red = worsening with EOT19
figure(2)
scatter(tg(:,1),tg(:,2),70,(rssFES14-rssEOT19)./rssFES14.*100,'fill')
hold on
polarmap_inverted
h=colorbar;
title(h,'[%]')
set(gca,'FontSize',15)
caxis([-30 30])
hold on
title('Improvement with EOT19 wrt FES2014')
plot_google_map('APIKey',API,'MapType','satellite')

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Plot RMS EOT19 for S2
% figure(3)
% scatter(tg(:,1),tg(:,2),70,rmsEOT19(:,3),'fill')
% hold on
% colormap jet
% colorbar
% caxis([0 2])
% hold on
% title('RMS_{EOT19}(S_2)')
% plot_google_map('APIKey',API,'MapType','satellite')

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% 
% % Plot RSS FES14
% figure
% scatter(tg(:,1),tg(:,2),70,rssFES14,'fill','MarkerEdgeColor','k')
% hold on
% colormap jet
% colorbar
% caxis([0 10])
% hold on
% title('RSS_{FES14}')
% plot_google_map('APIKey',API,'MapType','satellite')
% set(gca,'FontSize',15)
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% % Plot RSS FES12
% figure(5)
% scatter(tg(:,1),tg(:,2),70,rssFES12,'fill','MarkerEdgeColor','k')
% hold on
% colormap jet
% colorbar
% caxis([0 10])
% hold on
% title('RSS_{FES12}')
% plot_google_map('APIKey',API,'MapType','satellite')
% set(gca,'FontSize',15)
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Plot RSS FES04
% figure(6)
% scatter(tg(:,1),tg(:,2),70,rssFES04,'fill','MarkerEdgeColor','k')
% hold on
% colormap jet
% colorbar
% caxis([0 10])
% hold on
% title('RSS_{FES04}')
% plot_google_map('APIKey',API,'MapType','satellite')
% set(gca,'FontSize',15)

% % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% 
% % Plot RMS DTU16 for S2
% figure(5)
% scatter(tg(:,1),tg(:,2),70,rmsDTU16(:,3),'fill')
% hold on
% colormap jet
% colorbar
% caxis([0 10])
% hold on
% title('RMS_{DTU16}(S_2)')
% plot_google_map('APIKey',API,'MapType','satellite')
% 
% 
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Plot RMS EOT19 for M2
figure
scatter(tg(:,1),tg(:,2),70,rmsEOT19(:,1),'fill','MarkerEdgeColor','k')
hold on
colormap jet
colorbar
caxis([0 20])
hold on
title('RMS_{EOT19} M2')
plot_google_map('APIKey',API,'MapType','satellite')
set(gca,'FontSize',15)

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Plot RSS FES14
figure
scatter(tg(:,1),tg(:,2),70,rmsFES14(:,4),'fill','MarkerEdgeColor','k')
hold on
colormap jet
colorbar
caxis([0 30])
hold on
title('RMS_{FES14} K2')
plot_google_map('APIKey',API,'MapType','satellite')
set(gca,'FontSize',15)
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Plot RMS VS number of observations for the NASA missions
% 
% figure(8)
% for i=1:7
%     
%     subplot(4,2,i)
%     plot(nasa(:,3),rmsEOT19(:,i),'ob','MarkerSize',11,'LineWidth',1.2,'MarkerFaceColor',rgb('Aquamarine'))
%     hold on
%     axis tight
%     ylabel('RMS [cm]')
%     xlabel('nobs [-]')
%     title(['RMS for ' char(const{i}) ' VS nobs ESA'])
%     set(gca,'FontSize',10)
%     grid on
%     
% end
% 
% % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% 
% % Plot RMS VS number of observations for the ESA missions
% figure(9)
% for i=1:7
%     
%     subplot(4,2,i)
%     plot(esa(:,3),rmsEOT19(:,i),'ob','MarkerSize',11,'LineWidth',1.2,'MarkerFaceColor',rgb('Aquamarine'))
%     hold on
%     axis tight
%     ylabel('RMS [cm]')
%     xlabel('nobs [-]')
%     title(['RMS for ' char(const{i}) ' VS nobs ESA'])
%     set(gca,'FontSize',10)
%     grid on
%     
% end

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Plot RMS VS difference of number of observations: NASobs - ESA obs
% figure(10)
% for i=1:7
%     
%     subplot(4,2,i)
%     plot(nasa(:,3)-esa(:,3),rmsEOT19(:,i),'ob','MarkerSize',11,'LineWidth',1.2,'MarkerFaceColor',rgb('Aquamarine'))
%     hold on
%     axis tight
%     ylabel('RMS [cm]')
%     xlabel('\Delta nobs [-]')
%     title(['RMS for ' char(const{i}) ' VS \Delta obs'])
%     set(gca,'FontSize',10)
%     grid on
%     
% end

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Plot standard deviation of RMS for M2 for all the models to highlight
% where there are more differences. Use algorithm of Stammer et al. 2014

m2_am = [dtu16(:,3) eot11(:,3) eot19(:,3) fes14(:,3)];  % got48(:,3) tpxo8(:,3)
m2_ph = [dtu16(:,4) eot11(:,4) eot19(:,4) fes14(:,4)];   % got48(:,4) tpxo8(:,4)
avgM2_am = mean(m2_am,2);
avgM2_ph = mean(m2_ph,2);

nmod = 4; % number of models used for std

sdM2 = sqrt(sum(0.5*((m2_am.*cosd(m2_ph)-avgM2_am.*cosd(avgM2_ph)).^2 + ...
                     (m2_am.*sind(m2_ph)-avgM2_am.*sind(avgM2_ph)).^2),2)*1/nmod);
     
% figure(11)
% scatter(tg(:,1),tg(:,2),70,sdM2,'fill')
% hold on
% colormap colorcube(8)
% h=colorbar;
% title(h,'cm')
% set(gca,'FontSize',15)
% caxis([0 4])
% hold on
% title('STD RMS M2')
% plot_google_map('APIKey',API,'MapType','roadmap')

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% % compare sigma0
% s1 = dlmread('/DGFI93/home/piccioni/Dokumente/tidesGRID/results//DGFI8/H/work_gaia/tidesGRID/TGcomparison/EOTsla34_oflags16_iflags4_indonesia/_varCapSize/sigma0_NG');
% s2 = dlmread('/DGFI93/home/piccioni/Dokumente/tidesGRID/results//DGFI8/H/work_gaia/tidesGRID/TGcomparison/EOTsla34_oflags16_iflags4_indonesia/sigma0_NG');
% 
% mean(s2(:,3))




















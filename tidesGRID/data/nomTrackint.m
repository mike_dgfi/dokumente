% interpolation on nominal track
clear all
close all

addpath('/DGFI93/home/piccioni/MATLAB')

% input tracks
tracks = dlmread('idlonlatpass');

% select distance among track points (step between two longitudes)

% make for loop to work on many tracks
oldtrack=tracks(1,4);
start = 1;
newcoords=[];

for i=2:length(tracks(:,1))
    
    if tracks(i,4)==oldtrack
        xq=linspace(tracks(i-1,2),tracks(i,2),9);
        x=[tracks(i-1,2) tracks(i,2)];
        v=[tracks(i-1,3) tracks(i,3)];
        
        yq=interp1q(x',v',xq');
        
        newcoords = [newcoords; xq' yq oldtrack*ones(length(xq),1)];
        
    else
        
        oldtrack=tracks(i,4);
    
    end
    
end
% 
figure
plot(tracks(i-1:i,2),tracks(i-1:i,3),'o',xq,yq,'*')
grid on


% save denser tracks to file
iii = find(~isnan(newcoords(:,1)) & ~isnan(newcoords(:,2)));
newcoords = [(1:length(newcoords(iii,1)))' newcoords(iii,:)];
dlmwrite('nodes_CORR.txt',newcoords,'delimiter',' ')
% 
% 
% % plot to see if everything works fine

reg = dlmread('/DGFI93/home/piccioni/Dokumente/tidesAT/data/TGsel/tg_lonlat_pass_reg');

% newcoords=dlmread('/DGFI93/home/piccioni/Dokumente/tidesAT/data/nodes.txt');

jjj= find(tracks(:,2)>180);
tracks(jjj,2)=tracks(jjj,2)-360;

iii= find(newcoords(:,2)>180);
newcoords(iii,2)=newcoords(iii,2)-360;

kkk= find(reg(:,2)>180);
reg(kkk,2)=reg(kkk,2)-360;
% 
figure
plot(newcoords(:,2),newcoords(:,3),'.w','markerSize',15)
hold on
plot(tracks(:,2),tracks(:,3),'.r','markerSize',17)
hold on
plot(reg(:,2),reg(:,3),'ok','markerSize',7,'markerFaceColor','y')
plot_google_map('mapType','satellite')
% 
% 


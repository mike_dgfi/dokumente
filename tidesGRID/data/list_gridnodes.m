% prepare list of grid nodes with given: min_lon,max_lon,min_lat,max_lat,
% spacing_lon,spacing_lat.
% the degrees are given in decimals.
% longitude must be given within range [0 360].

clear all

% Open ocean
min_lon     = 324.5;
max_lon     = 330.5;
min_lat     = 4.5;
max_lat     = 9.5;
spacing_lon = 1/4;
spacing_lat = 1/4;

% North Sea
% min_lon     = -12;
% max_lon     = 3;
% min_lat     = 49;
% max_lat     = 60;
% spacing_lon = 1/8;
% spacing_lat = 1/8;

% create vector from given data
lon_data = min_lon:spacing_lon:max_lon;
lat_data = min_lat:spacing_lat:max_lat;

% create grid from vectors
[LON,LAT] = meshgrid(lon_data,lat_data);

% rearrange to vectors
lonV = reshape(LON,length(lon_data)*length(lat_data),1);
latV = reshape(LAT,length(lon_data)*length(lat_data),1);

% check everything is fine with plotting everything
% figure
% plot(lonV,latV,'.r')
% grid on

% save the grid as txt file with columns nodeID, lon, lat
% correct longitudes when <0 (case of areas over 0-meridian - e.g. North Sea)
ix = find(lonV<0);
lonV(ix) = lonV(ix)+360;
x = [(1:length(lonV))' lonV latV];
dlmwrite('nodes_grid.txt',x,'delimiter',' ')





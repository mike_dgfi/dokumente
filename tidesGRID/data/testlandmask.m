% test if the landmask for node cap works
clear all
close all
addpath('/DGFI93/home/piccioni/MATLAB')
API = 'AIzaSyCDPJKvX73RA0HBqRd1PIldhszqReBs0dg';

% lm = load('testcc');
lm1 = load('/DGFI93/home/piccioni/Dokumente/extractMVA/tpNew.txt');
lm2 = load('/DGFI93/home/piccioni/Dokumente/extractMVA/tpOld.txt');

tt = load('../../nominalTracks/topex_nomtrack.pos');
msk= load('lmask_check'); 

% idc=load('test');


% i360 = find(idc(:,2)>180);
% idc(i360,2)=idc(i360,2)-360;

i360 = find(lm1(:,1)>=180);
lm1(i360,1)=lm1(i360,1)-360;
i360 = find(lm2(:,1)>180);
lm2(i360,1)=lm2(i360,1)-360;



i360 = find(msk(:,1)>180);
msk(i360,1)=msk(i360,1)-360;

% figure
% plot(msk(:,1),msk(:,2),'.w','MarkerSize',5)
% hold on
% plot(tt(:,1),tt(:,2),'.g','MarkerSize',12)
% hold on
% plot(-5.75,50.25,'*r','MarkerSize',15)
% plot_google_map('APIKey',API,'MapType','satellite')
% iii=find(idc(:,1)==2480);
% 
figure
plot(tt(:,2),tt(:,3),'.w','MarkerSize',12)
hold on
plot(lm1(:,1),lm1(:,2),'.r','MarkerSize',12)
hold on
% plot(lm2(:,1),lm2(:,2),'.g','MarkerSize',8)
hold on
% scatter(idc(iii,2),idc(iii,3),20,idc(iii,1),'filled')
% colormap hsv
% hold on
% plot(msk(msk(:,3)==0,1),msk(msk(:,3)==0,2),'.c','MarkerSize',8)
% hold on
% plot(msk(msk(:,3)==1,1),msk(msk(:,3)==1,2),'.m','MarkerSize',8)
hold on
% plot(-4,50,'*y')
% plot(-16,46,'*y')
% plot(1.25,50.25,'*y')
plot(354.75-360, 52,'*y')
% plot(-5.75,50.25,'*y')
% plot(-2,50.5,'*y')
% plot(2.31645,51.355,'*y')
plot_google_map('APIKey',API,'MapType','satellite')
% 
% figure
% plot(lm(:,1),lm(:,4),'.r','MarkerSize',12)
% hold on
% plot(lm(:,1),lm(:,6),'.r','MarkerSize',12)
% hold on
% plot(lm(:,3),lm(:,4),'.r','MarkerSize',12)
% hold on
% plot(lm(:,3),lm(:,6),'.r','MarkerSize',12)
% hold on
% plot(lm(:,2),lm(:,5),'.b','MarkerSize',12)
% hold on
% plot(-5.75,50.25,'*g')
% hold on
% plot(tt(:,1),tt(:,2),'^m')
% grid on



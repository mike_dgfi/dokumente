
depth=-400:0;
radius1=-0.1625*depth+100;   % 200 km cap size (diameter) 
radius2=-0.2875*depth+50;    % 100 km cap size (diameter)
radius3=-0.35*depth+25;      % 50 km cap size (diameter)
radius4=-0.375*depth+15;     % 30 km cap size (diameter)
figure
% plot(depth,radius1,'b','LineWidth',2)
% hold on
plot(depth,radius2,'b','LineWidth',2)
% hold on
% plot(depth,radius3)
% hold on
% plot(depth,radius4)
hold on
plot(-600:-400,165*ones(201,1),'b','LineWidth',2)
% legend('50km','25km','15km')
grid on
set(gca,'Xdir','reverse')
set(gca,'fontSize',15)
ylim([50 180])
xlabel('Depth [ m ]')
ylabel('Radius size [ km ]')
yticks([50 90 130 165])
% title('Behaviour of different cap radii VS depth')


tr=dlmread('transect.txt');

id=find(tr(:,3)>=-400);
id2=find(tr(:,3)<=-400);

trnew=[tr(id,2) -0.1625*tr(id,3)+100; tr(id2,2) 300*ones(length(id2),1)];

figure
plot(tr(:,2),tr(:,3))
hold on
plot(trnew(:,1),trnew(:,2))
grid on

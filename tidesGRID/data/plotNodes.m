% plot nodes to see if everything is OK
clear all
close all

addpath('/DGFI93/home/piccioni/MATLAB')

d=dlmread('/DGFI93/home/piccioni/Dokumente/tidesAT/data/nodesMask.txt');
tg = dlmread('/DGFI93/home/piccioni/Dokumente/tidesAT/data/TGsel/tg_lonlat_pass_reg');


idx=find(d(:,2)>180);
d(idx,2)=d(idx,2)-360;

jjj= find(tg(:,2)>180);
tg(jjj,2)=tg(jjj,2)-360;

figure
plot(d(:,2),d(:,3),'.r','MarkerSize',20)
hold on
plot(tg(:,2),tg(:,3),'.c','markerSize',32)
title('Nodes + TG Locations')
plot_google_map('mapType','satellite')

 program maskForCap
 ! -----------------------------------------------------------------------------
 ! This program reads the node files and selects all the track points of each 
 ! mission which are within a certain cap size, and verifies that between the 
 ! node and the track points there is no land. If there is land, the point is
 ! discarded.
 ! Gaia Piccioni July 27th 2018
 ! -----------------------------------------------------------------------------
 
 implicit none
  
 integer                    :: jj,mm,controlcode,trec,sorb,iosmis,lolim,hilim,lolat,oldm
 integer                    :: id,depth,startcc,endcc,endpass,ic,szidx,iosmask,hilat
 integer                    :: iosor,iosdc,nf,ng,nh,ni,kk,m,lll,lb,hb,q,sp,deltacc1
 integer                    :: idxmask1,idxmask2,idxmask3,idxmask4,idxmask5,deltacc2
 integer                    :: idxmask6,idxmask7,idxmask8,idxmask9,idxmask10,deltacc3
 integer                    :: idxmask11,idxmask12,idxmask13,idxmask14,idxmask15
 integer                    :: idxmask16,idxmask17,idxmask18,idxmask19,idxmask20
 integer                    :: vrec1,vrec2,vrec3,vrec4
 integer*4                  :: hsat,pass,cyc,idx,lnin,ltin,landin,dc
 integer*4                  :: lon10,lat10,lat1,lat2
 integer*2                  :: cc,pp,j,jstart,jend
 integer*1                  :: oflags
 real*8                     :: llon,llat,xx,yy,xx1,l,r,land8,land9,land10,land11,land12
 real*8                     :: land1,land2,land3,land4,land5,land6,land7,land13,land14
 real*8                     :: land15,land16,land17,land18,land19,land20
 double precision           :: dlon,dlat,dx,dy,dist,erad,d2r,pi,i,distcoast
 double precision           :: lon,lat,deltalat,deltalon,plon
 double precision           :: xold,xnew,yold,ynew,glon,glat
 double precision           :: dl1,dl2,dl3,incpt,latlim
 character*12,dimension(10) :: ms,mnodes,misfold  
 character(LEN=3)           :: cy
 character(LEN=4)           :: ps
 character(LEN=8)           :: nodenr
 character(LEN=30)          :: pmva
 character(LEN=120)         :: nodepath,iorb,d2c,nodefile1,nodefile2,nodefile3,nodefile4
 logical                    :: iex
 
 ! define constants
 pi   = 3.141592654d0
 d2r  = pi/180.
 erad = 6371.
 cy='ccc'
 
 ! define destination folders and files
 misfold(1) ='TPnodes'
 misfold(2) ='J1nodes'
 misfold(3) ='J2nodes'
 misfold(4) ='ENnodes'
 misfold(5) ='E2nodes'
 misfold(6) ='E1Cnodes'
 misfold(7) ='E1Gnodes'
 misfold(8) ='TPEMnodes'
 misfold(9) ='J1EMnodes'
 misfold(10)='J2EMnodes'

 mnodes(1) ='TP.bin'
 mnodes(2) ='J1.bin'
 mnodes(3) ='J2.bin'
 mnodes(4) ='EN.bin'
 mnodes(5) ='E2.bin'
 mnodes(6) ='E1c.bin'
 mnodes(7) ='E1g.bin'
 mnodes(8) ='TPem.bin'
 mnodes(9) ='J1em.bin'
 mnodes(10)='J2em.bin'
 
 nodepath='/DGFI93/home/piccioni/Dokumente/tidesGRID/data/'
  
 ! define source folder (MVA structure)
 ms(1) ='topex'
 ms(2) ='jason1'
 ms(3) ='jason2'
 ms(4) ='envisat'
 ms(5) ='ers2'
 ms(6) ='ers1c'
 ms(7) ='ers1g'
 ms(8) ='topex_em'
 ms(9) ='jason1_em'
 ms(10)='jason2_em'
 
 pmva='/DGFI8/A/MVA/'
 
 ! define control code to accept/discard track points (controlcode=0 valid point, anything else: discarded) 
 controlcode=9
  
 ! open node file and read node by node 
 open(23, file='node_test2grid.txt')
 
 ! open ocean-land mask file, and declare the grid spacing (60 means 1 minute resolution: 1/60)
 open(24, file='olmaskWORLD_7min.bin',access='direct',recl=24, &
      form='unformatted',status='OLD',iostat=iosmask)
 sp=8

 do jj = 1,161
    
    read(23,*,end=9001) id,lon,lat,depth,distcoast
    
!     print*,'node nr. ', id,lon,lat

    do mm = 1,1 ! go through all missions
        
!         print*, 'mission loop'
        ! create file name for each mission and node and open file where to write indices, pass, cycle
        write(nodenr(1:8),'(I8.8)') id
        nodefile1=trim(nodepath)//'/'//trim(misfold(mm))//'/1_'// & 
                 nodenr//'_'//trim(mnodes(mm))
        nodefile2=trim(nodepath)//'/'//trim(misfold(mm))//'/2_'// & 
                 nodenr//'_'//trim(mnodes(mm))
        nodefile3=trim(nodepath)//'/'//trim(misfold(mm))//'/3_'// & 
                 nodenr//'_'//trim(mnodes(mm))
        nodefile4=trim(nodepath)//'/'//trim(misfold(mm))//'/4_'// & 
                 nodenr//'_'//trim(mnodes(mm))
                 
        nf=60+mm
        ng=70+mm
        nh=80+mm
        ni=90+mm
!         
        open (nf,file=nodefile1,access='direct',recl=8,&
            form='unformatted',status='REPLACE',iostat=iosmis) 
        open (ng,file=nodefile2,access='direct',recl=8,&
            form='unformatted',status='REPLACE',iostat=iosmis) 
        open (nh,file=nodefile3,access='direct',recl=8,&
            form='unformatted',status='REPLACE',iostat=iosmis) 
        open (ni,file=nodefile4,access='direct',recl=8,&
            form='unformatted',status='REPLACE',iostat=iosmis) 
            
        ! reset index of new binary file
        vrec1=1
        vrec2=1
        vrec3=1
        vrec4=1
        
        ! set starting and ending cycles, plus intermediate cycles to save to files
        ! also, decide at which pass to stop for each mission.
        if (mm .eq. 1) then         ! topex
            startcc = 1
            endcc   = 365
            deltacc1= 90
            deltacc2=180
            deltacc3=270
            endpass=254
        elseif (mm .eq. 2) then     ! jason-1
            startcc = 1
            endcc   = 259
            deltacc1=64
            deltacc2=128
            deltacc3=192
            endpass=254
        elseif (mm .eq. 3) then     ! jason-2
            startcc = 1
            endcc   = 298
            deltacc1=74
            deltacc2=148
            deltacc3=222
            endpass=254
        elseif (mm .eq. 4) then     ! envisat
            startcc = 6
            endcc   = 94
            deltacc1=20
            deltacc2=40
            deltacc3=80
            endpass=1002
        elseif (mm .eq. 5) then     ! ers-2
            startcc = 0
            endcc   = 85
            deltacc1=21
            deltacc2=42
            deltacc3=63
            endpass=1002
        elseif (mm .eq. 6) then     ! ers-1c
            startcc = 82
            endcc   = 101
            deltacc1=87
            deltacc2=92
            deltacc3=97
            endpass=1002
        elseif (mm .eq. 7) then     ! ers-1g
            startcc = 144
            endcc   = 156
            deltacc1=147
            deltacc2=150
            deltacc3=153
            endpass=1002
        elseif (mm .eq. 8) then     ! topex extended mission
            startcc = 368
            endcc   = 481
            deltacc1= 396
            deltacc2= 424
            deltacc3=452
            endpass=254
        elseif (mm .eq. 9) then     ! jason1 extended mission
            startcc = 262
            endcc   = 374
            deltacc1= 290
            deltacc2= 318
            deltacc3= 346
            endpass=254
        elseif (mm .eq. 10) then     ! jason2 extended mission
            startcc = 305
            endcc   = 319
            deltacc1= 309
            deltacc2= 312
            deltacc3= 315
            endpass=254
        endif        
        
        do cc=startcc,endcc  ! go through each cycle of each mission
        
            print*, 'cycle ',cc,'mission ',mm
            
            write(cy(1:3),'(I3.3)') cc
            
            do pp=1,endpass ! go through each pass of each cycle of each mission
!                     print*,pp       
                if (mm .le. 3 .or. mm .gt. 7) then
                    ps='ppp'
                    write(ps(1:3),'(I3.3)') pp
                elseif (mm .gt. 3 .and. mm .le. 7) then
                    ps='pppp'
                    write(ps(1:4),'(I4.4)') pp           
                endif   
                
                ! open orbit file 
                iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
                         '/'//cy//'_'//trim(ps)//'orbit.00'
                d2c=trim(pmva)//trim(ms(mm))//'/'//cy//&
                         '/'//cy//'_'//trim(ps)//'distcoast.00'         
!                 print*,iorb      
                INQUIRE(file=iorb,exist=iex,size=sorb)
!                 print*,sorb

                if (iex .and. sorb .ge. 18000) then
                    
                    open (30,file=iorb,access='direct',recl=13, &
                    form='unformatted',status='OLD',iostat=iosor)   
                    trec=sorb/13
                    
                    open (31,file=d2c,access='direct',recl=4, &
                    form='unformatted',status='OLD',iostat=iosdc)  
                    
                    
                    ! perform a binary search to find the boundaries close to node
                    ! first check if the track is ascending or descending
                    read(30,rec=10) lon10,lat1,hsat,oflags
                    read(30,rec=30) lon10,lat2,hsat,oflags
                    
                    if (lat1 .lt. lat2) then
                    
                        ! ascending
                    
                        ! low boundary
                        l=0.
                        r=trec
                        oldm=0
                        lll=lat*1000000-3000000
                        do while(l .le. r)
!                             print*,'lb',l,r,m,oldm
                            m=floor((l+r)/2)
                            read(30,rec=m) lon10,lat10,hsat,oflags                            
                            if (oldm .eq. m) then
                                lb=m
                                exit
                            elseif (lat10 .lt. lll) then
                                l=m+1
                                oldm=l
                            elseif (lat10 .gt. lll) then
                                r=m-1
                                oldm=r
                            else
                                lb=m
                                exit
                                
                            endif
                        end do
                        lb=m
                        
                        ! high boundary
                        l=0.
                        r=trec
                        oldm=0
                        lll=lat*1000000+3000000
                        do while(l .le. r)
!                             print*,'hb',l,r,m,oldm
                            m=floor((l+r)/2)
                            read(30,rec=m) lon10,lat10,hsat,oflags
                            if (oldm .eq. m) then
                                hb=m
                                exit
                            elseif (lat10 .lt. lll) then
                                l=m+1
                                oldm=l
                            elseif (lat10 .gt. lll) then
                                r=m-1
                                oldm=r
                            else
                                hb=m
                                exit
                            endif
                        end do
                        hb=m
                        
                    else
                    
                        ! descending
                        
                        ! low boundary
                        l=trec
                        r=0
                        oldm=0
                        lll=lat*1000000-3000000
                        do while(r .le. l)
                            m=floor((l+r)/2)
                            read(30,rec=m) lon10,lat10,hsat,oflags                            
                            if (oldm .eq. m) then
                                hb=m
                                exit
                            elseif (lat10 .lt. lll) then
                                l=m+1
                                oldm=m
                            elseif (lat10 .gt. lll) then
!                             print*,'aa'
                                r=m-1
                                oldm=m
                            else
                                hb=m
                                exit
                            endif
                        end do  
                        hb=m
                        
                        ! high boundary
                        l=trec
                        r=0.
                        lll=lat*1000000+3000000
                        oldm=0
                        do while(r .le. l)
!                             print*,'lb',l,r,m,oldm
                            m=floor((l+r)/2)
                            read(30,rec=m) lon10,lat10,hsat,oflags
                            if (oldm .eq. m) then
                                lb=m
                                exit                            
                            elseif (lat10 .lt. lll) then
                                l=m+1
                                oldm=m
                            elseif (lat10 .gt. lll) then
                                r=m-1
                                oldm=m
                            else
                                lb=m
                                exit                                
                            endif
                        end do
                        lb=m
                    endif
                                     
!                     print*,lb,hb
                    ! now go through the selected indices
                    jstart=0
                    jend=0
                    do j=lb,hb
                                                
                        ! ----- read file and check what are the data to account 
                        read(30,rec=j) lon10,lat10,hsat,oflags
                        read(31,rec=j) dc
                        glon = lon10/1000000.
                        glat = lat10/1000000.

                        ! compute distance node - track point
                        dl1 = abs(lon - glon)
                        dl2 = abs(lon - glon + 360)
                        dl3 = abs(lon - glon - 360)
                        dlon = min(dl1,dl2,dl3)*d2r
                        dlat = abs(lat - glat)*d2r     
                        dx   = dlon*erad*cos(glat*d2r)
                        dy   = dlat*erad
                        dist = sqrt(dx*dx+dy*dy)    
                        
                        if (dist .le. 250 .and. dc .ge. 3000) then ! accept only points within 400 km from node
                            
!                                 print*, j,'distance below 250km'
                            
                            if (distcoast .ge. 250 .or. dist .lt. distcoast) then ! if the node is farther than 200km from the coast, write the point to the file
                                
!                                     print*, 'distance to coast over 200km'!,dist,distcoast
                                if (jstart .eq. 0) then
                                    jstart=j
                                    jend=j           
                                elseif (jstart .ne. 0 .and. j .eq. jend+1) then
                                    jend=j
                                elseif (jstart .ne. 0 .and. j .gt. jend+1) then
                                
                                    if (cc .le. deltacc1) then
                                        write(nf,rec=vrec1) pp,cc,jstart,jend
                                        vrec1=vrec1+1
                                    elseif (cc .le. deltacc2 .and. &
                                            cc .gt. deltacc1) then
                                        write(ng,rec=vrec2) pp,cc,jstart,jend
                                        vrec2=vrec2+1
                                    elseif (cc .le. deltacc3 .and. &
                                            cc .gt. deltacc2) then
                                        write(nh,rec=vrec3) pp,cc,jstart,jend
                                        vrec3=vrec3+1       
                                    elseif (cc .gt. deltacc3) then
                                        write(ni,rec=vrec4) pp,cc,jstart,jend
                                        vrec4=vrec4+1
                                    endif
                                    jstart=j
                                    jend=j
                                    
                                endif

                            else ! if the node is close to the coast, check that there is no land between the node and the track points
                                
!                                  print*, j,'observation distance to coast below 250km'

                                ! check that the longitudes heve no problem at the extremes: 0,360
                                if (lon .gt. 300 .and. glon .lt. 10) then
                                    deltalon=lon-glon-360.
                                    xold=lon-360.
                                    plon=glon
                                elseif (lon .lt. 10 .and. glon .gt. 300) then
                                    deltalon=lon-glon+360.
                                    xold=lon
                                    plon=glon-360.
                                else
                                    deltalon=lon-glon    
                                    xold=lon  
                                    plon=glon
                                endif

                                deltalat=lat-glat                                 
                                yold=lat
                                    
                                ! do-while loop that goes through intervals between node and track point
                                ! note that I used anint to round the last digits, otherwise I don't 
                                ! manage to compare xold and glon properly
                                
                                q=20
                                    
                                idxmask1 = ceiling((xold-deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+deltalat/q &
                                        -1/sp)*sp)
                                read(24,rec=idxmask1) llon,llat,land1
!                                 print*,j,llon,llat,land1
                                
                                idxmask2 = ceiling((xold-2*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+2*deltalat/q &
                                        -1/sp)*sp)
                                read(24,rec=idxmask2) llon,llat,land2
!                                 print*,j,llon,llat,land2
                                 
                                idxmask3 = ceiling((xold-3*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+3*deltalat/q &
                                        -1/sp)*sp)
                                read(24,rec=idxmask3) llon,llat,land3  
!                                 print*,j,llon,llat,land3
!                                                                  
                                idxmask4 = ceiling((xold-4*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+4*deltalat/q &
                                        -1/sp)*sp)
                                read(24,rec=idxmask4) llon,llat,land4
!                                 print*,j,llon,llat,land4

                                idxmask5 = ceiling((xold-5*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+5*deltalat/q &
                                        -1/sp)*sp)
                                read(24,rec=idxmask5) llon,llat,land5
!                                 print*,j,llon,llat,land5
                                
                                idxmask6 = ceiling((xold-6*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+6*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask6) llon,llat,land6
!                                 print*,j,llon,llat,land6 
                                
                                idxmask7 = ceiling((xold-7*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+7*deltalat/q &
                                        -1/sp)*sp)                             
                                read(24,rec=idxmask7) llon,llat,land7
!                                 print*,j,llon,llat,land7
                                
                                idxmask8 = ceiling((xold-8*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+8*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask8) llon,llat,land8
!                                 print*,j,llon,llat,land8 
                                
                                idxmask9 = ceiling((xold-9*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+9*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask9) llon,llat,land9 
!                                 print*,j,llon,llat,land9
                                
                                idxmask10 = ceiling((xold-10*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+10*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask10) llon,llat,land10
!                                 print*,j,llon,llat,land10 
                                
                                idxmask11 = ceiling((xold-11*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+11*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask11) llon,llat,land11
!                                 print*,j,llon,llat,land11 
                                
                                idxmask12 = ceiling((xold-12*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+12*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask12) llon,llat,land12
!                                 print*,j,llon,llat,land12
                                
                                
                                idxmask13 = ceiling((xold-13*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+13*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask13) llon,llat,land13
!                                 print*,j,llon,llat,land13 
                                
                                idxmask14 = ceiling((xold-14*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+14*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask14) llon,llat,land14
!                                 print*,j,llon,llat,land14 
                                
                                idxmask15 = ceiling((xold-15*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+15*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask15) llon,llat,land15
!                                 print*,j,llon,llat,land15
                                
                                idxmask16 = ceiling((xold-16*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+16*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask16) llon,llat,land16
!                                 print*,j,llon,llat,land16
                                
                                idxmask17 = ceiling((xold-17*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+17*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask17) llon,llat,land17
!                                 print*,j,llon,llat,land17                             

                                idxmask18 = ceiling((xold-18*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+18*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask18) llon,llat,land18
!                                 print*,j,llon,llat,land18
                                
                                idxmask19 = ceiling((xold-19*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+19*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask19) llon,llat,land19
!                                 print*,j,llon,llat,land19
                                
                                idxmask20 = ceiling((xold-20*deltalon/q)*sp) + 1 + &
                                        (360*sp + 1)*ceiling((90-yold+20*deltalat/q &
                                        -1/sp)*sp) 
                                read(24,rec=idxmask20) llon,llat,land20
!                                 print*,j,llon,llat,land20

                                
                                ! if there is no land between node and track point, save to file                                            
                                if (land1 .eq. 0 .and. land2 .eq. 0 .and. &
                                    land3 .eq. 0 .and. land4 .eq. 0 .and. &  
                                    land5 .eq. 0 .and. land6 .eq. 0 .and. &
                                    land7 .eq. 0 .and. land8 .eq. 0 .and. &
                                    land9 .eq. 0 .and. land10 .eq. 0 .and. &
                                    land11 .eq. 0 .and. land12 .eq. 0 .and. &
                                    land13 .eq. 0 .and. land14 .eq. 0 .and. &
                                    land15 .eq. 0 .and. land16 .eq. 0 .and. &
                                    land17 .eq. 0 .and. land18 .eq. 0 .and. &
                                    land19 .eq. 0 .and. land20 .eq. 0) then
                                
!                                    print*,land1,land2
                                    if (jstart .eq. 0) then
                                        jstart=j
                                        jend=j           
                                    elseif (jstart .ne. 0 .and. j .eq. jend+1) then
                                        jend=j
                                    elseif (jstart .ne. 0 .and. j .gt. jend+1) then
                                    
                                        if (cc .le. deltacc1) then
                                            write(nf,rec=vrec1) pp,cc,jstart,jend
                                        vrec1=vrec1+1
                                        elseif (cc .le. deltacc2 .and. &
                                                cc .gt. deltacc1) then
                                            write(ng,rec=vrec2) pp,cc,jstart,jend
                                        vrec2=vrec2+1
                                        elseif (cc .le. deltacc3 .and. &
                                                cc .gt. deltacc2) then
                                            write(nh,rec=vrec3) pp,cc,jstart,jend
                                        vrec3=vrec3+1      
                                        elseif (cc .gt. deltacc3) then
                                            write(ni,rec=vrec4) pp,cc,jstart,jend
                                        vrec4=vrec4+1
                                        endif
                                        jstart=j
                                        jend=j
                                        
                                    endif
                                             
                                    
                                endif ! end condition that checks if there is land between node and track point                                                                  
                            
                            endif ! end condition that checks that the node is farther than 200km from the coast
                        
                        else
                            
                            if (jstart .ne. 0 .and. jend .le. j-1) then
                                
                                    if (cc .le. deltacc1) then
                                        write(nf,rec=vrec1) pp,cc,jstart,jend
                                        vrec1=vrec1+1
                                    elseif (cc .le. deltacc2 .and. &
                                            cc .gt. deltacc1) then
                                        write(ng,rec=vrec2) pp,cc,jstart,jend
                                        vrec2=vrec2+1
                                    elseif (cc .le. deltacc3 .and. &
                                            cc .gt. deltacc2) then
                                        write(nh,rec=vrec3) pp,cc,jstart,jend
                                        vrec3=vrec3+1    
                                    elseif (cc .gt. deltacc3) then
                                        write(ni,rec=vrec4) pp,cc,jstart,jend
                                        vrec4=vrec4+1
                                    endif
                                    jstart=0
                                    jend=0
                            endif
                                                        
                        endif ! end condition to accept only points within 300 km from node
                    
                    enddo ! end loop that goes through each track point
                    
!                     if (jstart .eq. 0 .and. jend .le. j-1) then
!                         jstart=j
!                         jend=j           
!                     elseif (jstart .ne. 0 .and. j .eq. jend+1) then
!                         jend=j
!                     elseif (jstart .ne. 0 .and. j .gt. jend+1) then
!                     
!                         if (cc .le. deltacc1) then
!                             write(nf) pp,cc,jstart,jend
!                         elseif (cc .le. deltacc2 .and. &
!                                 cc .gt. deltacc1) then
!                             write(ng) pp,cc,jstart,jend
!                         elseif (cc .le. deltacc3 .and. &
!                                 cc .gt. deltacc2) then
!                             write(nh) pp,cc,jstart,jend       
!                         elseif (cc .gt. deltacc3) then
!                             write(ni) pp,cc,jstart,jend
!                         endif
!                         jstart=j
!                         jend=j
!                         
!                     endif

                    close(30) 
                    close(31)
                    
                endif ! end condition if orbit file exists
                
            enddo ! end loop that goes through all passes
        
        enddo ! end loop that goes through all cycles
                
        close(nf)
        close(ng)
        close(nh)
        close(ni)
    enddo ! end loop that goes through all missions
    
!     deallocate(idxmask)
 enddo ! end loop that goes though nodes
 close(23)
 close(24)
9001 continue 
 
 end program maskForCap

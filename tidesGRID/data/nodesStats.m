% open binary file
clear all
close all
id=fopen('J2nodes/00000003_J2.bin','r');
A=fread(id,'integer*2');
sa=size(A);
a1=reshape(A,4,sa(1)/4);
a2=a1';

up=unique(a2(:,1));

for i=1:length(up)
    
    ip=find(a2(:,1)==up(i));
    avrec=mean(a2(ip,[3 4]),2);
    disp(['pass  ' num2str(up(i)) ' ' num2str(mean(avrec)) ' ' num2str(std(a2(ip,3))) ' ' num2str(std(a2(ip,4)))])
%     errorbar(up(i),mean(avrec),std(a2(ip,3)),std(a2(ip,4)));
%     hold on
%     grid on
end
% select track points from nominal tracks, within regions defined in file
% tg_lonlat_pass_reg

clear all
close all

addpath('/DGFI93/home/piccioni/MATLAB/')

ntr = dlmread('/DGFI93/home/piccioni/Dokumente/nominalTracks/topex_nomtrack.pos');
reg = dlmread('/DGFI93/home/piccioni/Dokumente/tidesAT/data/TGsel/tg_lonlat_pass_reg');

i360=find(ntr(:,2)<0);
ntr(i360,2)=ntr(i360,2)+360;

regsel=[];

for i=1:length(reg(:,1))
    
    ix = find(ntr(:,1)==reg(i,4) & ntr(:,2)>=reg(i,5)-1 & ntr(:,2)<=reg(i,6)+1 & ...
              ntr(:,3)>=reg(i,7)-1 & ntr(:,3)<=reg(i,8)+1);
    
    regsel = [regsel; ntr(ix,2) ntr(ix,3) ntr(ix,1)];
          
end

regsel=[(1:length(regsel(:,1)))' regsel];

dlmwrite('idlonlatpass',regsel,'delimiter',' ')


% plot to verify
iii= find(regsel(:,2)>180);
regsel(iii,2)=regsel(iii,2)-360;

jjj= find(reg(:,2)>180);
reg(jjj,2)=reg(jjj,2)-360;

figure
plot(regsel(:,2),regsel(:,3),'.w','markerSize',12)
hold on
plot(reg(:,2),reg(:,3),'.r','markerSize',12)
plot_google_map('mapType','satellite')
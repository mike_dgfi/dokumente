% compare indices and select only certain data
clear all
close all

sel=load('/DGFI93/home/piccioni/Dokumente/tidesAT/data/TGsel/coastal.tgc');
all=dlmread('/DGFI93/home/piccioni/Dokumente/tidesAT/data/TGsel/tg_lonlat_pass_reg');

subset= [];

for j=1:length(all(:,1))
    
    idx = find(sel(:,1)==all(j,:));
    
    if ~isempty(idx)  
        subset=[subset; j];
    end
    
end


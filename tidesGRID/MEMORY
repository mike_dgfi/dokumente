In this note I have done some calculations about the amount of storage memory 
I need for the indexing files (see folders XXnodes in the data_newGrid folder, 
with XX the name of the mission).

For the grid_v3 we have in total 463667 nodes over the ocean (see file 
vertices_ocean.txt in folder /DGFI8/L/sea_level_2017/grid_v3).

At the moment, for the Topex mission (folder TPnodes), I have performed the 
indexing for 10735 nodes, which correspond to 170.8 MB.
It means that for all the 463667 nodes I would need around 7.4 GB of memory
(170 MB * 463667 / 10661 = 7.393 GB)

To this, the other missions must be added.
I can base my calculation on only 1278 indexed nodes at the moment. 
I will write the size of the folder up to now, and the size after making the 
global indexing (I rounded to ceiling) :

    Envisat : 30.3 MB, therefore 10.9 GB      +
    ERS2    : 28.5 MB, therefore 10.4 GB      +
    ERS1c   : 6.2 MB , therefore  2.3 GB      +
    ERS1g   : 4.3 MB , therefore  1.6 GB      +
    Jason1  : 31.4 MB, therefore 11.4 GB      +
    Jason2  : 37.6 MB, therefore 13.7 GB      +
    Jason3  : 11.4 MB, therefore  4.2 GB      +
    TopexEM : 13.4 MB, therefore  4.9 GB      +
    Jason1EM: 13.7 MB, therefore  5   GB      +
    Jason2EM: 2.6 MB , therefore  1   GB      +

--------------------------------------------------------------------------
Total for all missions, including Topex       =   73 GB

So, let's say I need 100 GB.
This is because for the moment I am working close to the coast, and maybe I 
will have less tracks to index, while on a global scale I will be over open 
ocean, and maybe my files are larger. Anyway, 100 GB look less than what I 
expected at the beginning (TB of data). 
According to what we said with Christian, it should be more than ok to do 
everything in Octopus, which right now has 1.5 TB data free.


-----------------------------------------------------------------------------------------

Written by Gaia Piccioni   $ 24.01.2019 $


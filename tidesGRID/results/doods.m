function [tau,s,h,p,N,pp] = doods(mjd)
%
% This function computes the dooson arguments used to derive the harmonic
% constants:
% input:  time in modified julian days: mjd
% output: astronomical arguments tau,s,h,p,N,pp 
% see 
%
d2r    = pi/180;
circle = 360;
t      = (mjd(:)-51544.5)./365250;
Tu0    = (fix(mjd(:))-51544.5)./36525;

% from: http://www.cv.nrao.edu/~rfisher/Ephemerides/times.html#ref9
% compute mean sidereal time
gmst0 = (24110.54841 + Tu0.*(8640184.812866 + Tu0.*(0.093104 - Tu0*6.2e-6)))/86400;

% from: GPS Satellite Surveying, A.Leick,L.Rapoport, D. Tatarnikov
% PAGE 148 for r and GMST definition
% r is the ratio of the mean sidereal time to UT1
r    = 1.002737909350795 + Tu0.*5.9006e-11 - Tu0.*Tu0.*5.9e-15;
% disp(length(r))
% disp(length(mjd))
rmod = r.*mod(mjd,1);

% 15 are the degrees that must be multiplied to gmst to convert
% hours to degrees and 15*24(hours)=360(circle) because we need
% the hour angle
gmst  = mod(2*pi*(gmst0+rmod),2*pi);

s   =  mod((218.31664562999 + (4812678.81195750 + (-0.14663889+( 0.00185140 - 0.00015355*t).*t).*t).*t),circle)*d2r;
h   =  mod((280.46645016002 + ( 360007.69748806 + ( 0.03032222+( 0.00002000 - 0.00006532*t).*t).*t).*t),circle)*d2r;
p   =  mod(( 83.35324311998 + (  40690.13635250 + (-1.03217222+(-0.01249168 + 0.00052655*t).*t).*t).*t),circle)*d2r;

% minus in front of N, because the longitude of Moon's perigee is -N 
% see Tamura 1987, page 6816.
N   = -mod((234.95544499000 + (  19341.36261972 + (-0.20756111+(-0.00213942 + 0.00016501*t).*t).*t).*t),circle)*d2r;
pp  =  mod((282.93734098001 + (     17.19457667 + ( 0.04568889+(-0.00001776 - 0.00003323*t).*t).*t).*t),circle)*d2r;

tau = gmst + pi - s;
% 

% tau(tau < 0) = tau(tau < 0) + circle;
% s(s < 0)     = s(s < 0)     + circle;
% h(h < 0)     = h(h < 0)     + circle;
% p(p < 0)     = p(p < 0)     + circle;
% N(N < 0)     = N(N < 0)     + circle;
% pp(pp < 0)   = pp(pp < 0)   + circle;

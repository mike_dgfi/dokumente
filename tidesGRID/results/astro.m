function [utheta,f] = astro_19032019(dw,doodson,t)
% this function computes the nodal corrections from the astronomical frequencies 
% derived in doods.m
%
% input: time in modified Julian days, t.
%        Doodson numbers (doodson), string with constituent's name as cell array (dw).
% output: cosine argument corrected for u (utheta= frequency*time + u) , and the correction f 
%
% Formula to keep in mind:
%         A*f*cos(utheta-P),where A=amplitude and P=phase
%
% N. B. Here we add also the Phase adjustments, coming from the extended
% doodson number (EDN). The values are taken from table 4.1 of book: 
% sea level science (Pugh and Woodworth 2014), chapter 4, page 65.
%
% Gaia Piccioni   $ 29.01.2017 $
%
% The equations for Nodal Corrections have been updated as per the IHO
% document that can be found in this link - 
% https://www.iho.int/mtg_docs/com_wg/IHOTC/IHOTC8/Product_Spec_for_Exchange_of_HCs.pdf

pihalf = pi/2;
d2r    = pi/180;

% get theta from the astronomical arguments computed in doods
[tau,s,h,p,N,pp] = doods(t);
% disp(size(doodson))
% disp(length(tau))

theta = doodson*[tau s h p N pp]';
theta=theta';
%disp(size(theta))

f      = ones(length(t),length(dw));
utheta = zeros(length(t),length(dw));


for i=1:length(dw)
%     disp(dw{i})
    
    switch dw{i}
        
        
%         disp(lenght(N))
        
        case {'Mm','MM', 'MTM'}
            utheta(:,i) = theta(:,i);
            f(:,i)      = 1.000 - 0.1311*cos(N) + 0.0538*cos(2*p) + 0.0205*cos(2*p-N);
                        
        case {'Mf','MF'}
            utheta(:,i) = theta(:,i) - d2r*(23.7*sin(N) - 2.7*sin(2*N) + 0.4*sin(3*N));
            f(:,i)      = 1.084 + 0.415*cos(N) + 0.039*cos(2*N);
        
        case {'K1'}
            utheta(:,i) = theta(:,i) -  d2r*8.86*sin(N) + d2r*0.68*sin(2*N) - 0.07*sin(3*N) + pihalf;
            f(:,i)      = 1.006 + 0.115*cos(N) - 0.0088*cos(2*N) + 0.0006*cos(3*N);
        
        case {'O1','Q1','2Q1','RHO1','SIG1'}
            utheta(:,i) = theta(:,i) + d2r*(10.8*sin(N) - 1.34*sin(2*N) + 0.19*sin(3*N)) - pihalf;
            f(:,i)      = 1.0176 + 0.1876*cos(N) -0.0147*cos(2*N);
        
        case {'J1','PHI1','CHI1','THE1'} % from IHO, also the Extended Doodson Number, +90 (book of Woodworth -90) 
            utheta(:,i) = theta(:,i) - d2r*(12.94*sin(N) - 1.34*sin(2*N) + 0.19*sin(3*N)) + pihalf;
            f(:,i)      = 1.1029 + 0.1676*cos(N) - 0.0170*cos(2*N) + 0.0016*cos(3*N);
            
            
        case {'M2','N2','MI2','NI2','2N2','MA2','MB2','MS4','EP2'}
            utheta(:,i) = theta(:,i) - 2.14*d2r*sin(N);
            f(:,i)      = 1.0007 - 0.0373*cos(N) + 0.0002*cos(2*N);
        
        case {'K2'}
            utheta(:,i) = theta(:,i) - d2r*(17.74*sin(N) - 0.68*sin(2*N) + 0.04*sin(3*N));
            f(:,i)      = 1.0246 + 0.2863*cos(N) + 0.0083*cos(2*N) - 0.0015*cos(3*N);
        
        case {'L2'}
            cosu = 1.  - 0.2505*cos(2*p) - 0.1102*cos(2*p-N) - 0.0156*cos(2*(p-N)) - 0.037*cos(N);
            sinu =     - 0.2505*sin(2*p) - 0.1102*sin(2*p-N) - 0.0156*sin(2*(p-N)) - 0.037*sin(N);
            utheta(:,i) = theta(:,i) +  atan2(sinu,cosu) + pi;
            f(:,i)      = sqrt(cosu.*cosu+ sinu.*sinu);
        
        case {'M1'}
            cosu = 2*cos(p) + 0.4*cos(p-N);
            sinu =   sin(p) + 0.2*sin(p-N);
            utheta(:,i) = theta(:,i) +  atan2(sinu,cosu) + pi;
            f(:,i)      = sqrt(cosu.*cosu+ sinu.*sinu);
        
        case {'PI1','P1'}
            utheta(:,i) = theta(:,i) - pihalf;            
        
        case{'OO1'} % Acc. to document, OO1 has the same nodal correction as the constituent KQ1. 
            % KQ1 can be written as K2-Q1(also explained in the document). 
            % Therefore, u(OO1) = u(KQ1) = u(K2) - u(Q1); f(OO1) = f(KQ1) = f(K2)*f(Q1)
            utheta(:,i) = theta(:,i) - d2r*(28.54*sin(N) - 2.02*sin(2*N) + 0.23*sin(3*N))+ pihalf;
            f(:,i)      = (1.0246 + 0.2863*cos(N) + 0.0083*cos(2*N) - 0.0015*cos(3*N)).*(1.0176 + 0.1876*cos(N) -0.0147*cos(2*N));
        
        case {'R2'}
            utheta(:,i) = theta(:,i) + pi;
            
        case {'LM2'}
            utheta(:,i) = theta(:,i) - 2.14*d2r*sin(N) + pi;
            f(:,i)      = 1.0007 - 0.0373*cos(N) + 0.0002*cos(2*N);
        
        case {'M4','MN4','N4'}
            utheta(:,i) = theta(:,i) - 2*2.14*d2r*sin(N);
            f(:,i)      = (1.0007 - 0.0373*cos(N) + 0.0002*cos(2*N)).^2;
            
        case {'M6'}
            utheta(:,i) = theta(:,i) - 3*2.14*d2r*sin(N);
            f(:,i)      = (1.0007 - 0.0373*cos(N) + 0.0002*cos(2*N)).^3;
            
        case {'M8'}
            utheta(:,i) = theta(:,i) - 4*2.14*d2r*sin(N);
            f(:,i)      = (1.0007 - 0.0373*cos(N) + 0.0002*cos(2*N)).^4;
        
        case {'S2','Sa','SA','SSA','Ssa','T2','S4'}
            utheta(:,i) = theta(:,i);  
        
        case {'MSF','MSQ'}
            utheta(:,i) = theta(:,i) + 2.14*d2r*sin(N);
            f(:,i)      = 1.0007 - 0.0373*cos(N) + 0.0002*cos(2*N);
            
        case {'MKS'}
            utheta(:,i) = theta(:,i) - d2r*(19.88*sin(N) - 0.68*sin(2*N) + 0.04*sin(3*N));
            f(:,i)      = (1.0007 - 0.0373*cos(N) + 0.0002*cos(2*N)).*(1.0246 + 0.2863*cos(N) + 0.0083*cos(2*N) - 0.0015*cos(3*N));
       
        case {'M3'}  % from https://www.iho.int/mtg_docs/com_wg/IHOTC/IHOTC8/Product_Spec_for_Exchange_of_HCs.pdf
            utheta(:,i) = theta(:,i) - 3.21*d2r*sin(N) + pi;
            f(:,i)      = (sqrt(1.0007 - 0.0373*cos(N) + 0.0002*cos(2*N))).^3; 
            
        case{'S1','S3'}  % they differ respectively 10 and 30 degrees wrt Ray's results, as he uses a "Doodson Phase" method
            utheta(:,i) = theta(:,i)+pi;
            f(:,i)      = 1;
               
    end
    
end


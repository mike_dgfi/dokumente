% script to test Least squares form different missions.
clear all
close all

% read SLA and mission offset data
d = dlmread('SLA_324.txt');
mis = d(:,1); % misison number (see eotLaunchALES for reference numbers)
lon = d(:,2);
lat = d(:,3);
mjd = d(:,4); % time in modified julian days
sla = d(:,5);
wgs = d(:,6); % weights according to distance to node
off = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/off');


% read harmonic constants data and sla trend
m2 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/M2');
s2 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/S2');
n2 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/N2');
k2 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/K2');
k1 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/K1');
o1 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/O1');
p1 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/P1');
q1 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/Q1');
mm = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/MM');
mf = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/MF');
m4 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/M4');
s1 = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/S1');
nn2= dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/2N2');
aa = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/SA');
ss = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/SSA');
trend = dlmread('/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_noavg_nSea_d2cALES_oneoffset/slaTrend');

% plot SLA data, each mission has a different color
um = unique(mis);

figure(1)
for i=1:length(um)
    
    im = find(mis==um(i));
    plot(mjd(im)./365.24+1858.9639,sla(im),'lineWidth',1.5)
    hold on
    
end
grid on
legend('TP','J1','J2','EN','E2','E1c','E1g','TPem','J1em','J2em','J3')

% plot least squares fit onto timeseries
% -------------------------------------------------------------------------
% but first, make prediction
% select the tidal constituents to use in this least squares:
dw ={'M2'; 'K1'; 'N2'; 'O1'; 'P1'; 'Q1'; 'K2'; 'S2'};

% read the tidal catalogue according to the tidal constituents chosen,
% in order to get the 6 Doodson numbers:
fcat=readtable('/win/public/Piccioni/tideGaugeDataNEW/catHW95');

doodson = zeros(length(dw),6);
for j=1:length(dw)
    
    istr = find(strcmp(dw{j},table2array(fcat(:,1))));
    
    doodson(j,:) = [fcat.Var2(istr) fcat.Var3(istr) fcat.Var4(istr) ...
        fcat.Var5(istr) fcat.Var6(istr) fcat.Var7(istr)];
    
end

% Call Doodson script, given time in mjd and doodson numbers
% if there are more constituents and more observations than 1,
% utheta and f are matrices, with dimension rows=observation,
% columns=number of constituents.
[utheta,f]=astro(dw,doodson,mjd-51544.5);

fit = zeros(length(mjd),1);
for i=1:length(um)
    im = find(mis==um(i));
    
    fit(im) = off(um(i)) +trend(:,3)*(mjd(im)-51544.5d0)./365.2422d0 + ...
        f(im,1).*cos(utheta(im,1)).*m2(3) + f(im,1).*sin(utheta(im,1)).*m2(4)+ ...
        f(im,2).*cos(utheta(im,2)).*k1(3) + f(im,2).*sin(utheta(im,2)).*k1(4)+ ...
        f(im,3).*cos(utheta(im,3)).*n2(3) + f(im,3).*sin(utheta(im,3)).*n2(4)+ ...
        f(im,4).*cos(utheta(im,4)).*o1(3) + f(im,4).*sin(utheta(im,4)).*o1(4)+ ...
        f(im,5).*cos(utheta(im,5)).*p1(3) + f(im,5).*sin(utheta(im,5)).*p1(4)+ ...
        f(im,6).*cos(utheta(im,6)).*q1(3) + f(im,6).*sin(utheta(im,6)).*q1(4)+ ...
        f(im,7).*cos(utheta(im,7)).*k2(3) + f(im,7).*sin(utheta(im,7)).*k2(4)+ ...
        f(im,8).*cos(utheta(im,8)).*s2(3) + f(im,8).*sin(utheta(im,8)).*s2(4)+ ...
        cos(4*pi*(mjd(im)-51544.5)./365.2422).*ss(3) + sin(4*pi*(mjd(im)-51544.5)./365.2422).*ss(4)+ ...
        cos(2*pi*(mjd(im)-51544.5)./365.2422).*aa(3) + sin(2*pi*(mjd(im)-51544.5)./365.2422).*aa(4);
    
end

% fit = off(3) + trend(:,3)*(mjd-51544.5d0)./365.2422d0 + ...
%     f(:,1).*cos(utheta(:,1)).*m2(3) + f(:,1).*sin(utheta(:,1)).*m2(4)+ ...
%     f(:,2).*cos(utheta(:,2)).*k1(3) + f(:,2).*sin(utheta(:,2)).*k1(4)+ ...
%     f(:,3).*cos(utheta(:,3)).*n2(3) + f(:,3).*sin(utheta(:,3)).*n2(4)+ ...
%     f(:,4).*cos(utheta(:,4)).*o1(3) + f(:,4).*sin(utheta(:,4)).*o1(4)+ ...
%     f(:,5).*cos(utheta(:,5)).*p1(3) + f(:,5).*sin(utheta(:,5)).*p1(4)+ ...
%     f(:,6).*cos(utheta(:,6)).*q1(3) + f(:,6).*sin(utheta(:,6)).*q1(4)+ ...
%     f(:,7).*cos(utheta(:,7)).*k2(3) + f(:,7).*sin(utheta(:,7)).*k2(4)+ ...
%     f(:,8).*cos(utheta(:,8)).*s2(3) + f(:,8).*sin(utheta(:,8)).*s2(4)+ ...
%     f(:,8).*cos(utheta(:,8)).*s1(3) + f(:,8).*sin(utheta(:,8)).*s1(4)+ ...
%     f(:,8).*cos(utheta(:,8)).*m4(3) + f(:,8).*sin(utheta(:,8)).*m4(4)+ ...
%     f(:,8).*cos(utheta(:,8)).*mm(3) + f(:,8).*sin(utheta(:,8)).*mm(4)+ ...
%     f(:,8).*cos(utheta(:,8)).*mf(3) + f(:,8).*sin(utheta(:,8)).*mf(4)+ ...
%     f(:,8).*cos(utheta(:,8)).*nn2(3) + f(:,8).*sin(utheta(:,8)).*nn2(4)+ ...
%     cos(4*pi*(mjd-51544.5)./365.2422).*ss(3) + sin(4*pi*(mjd-51544.5)./365.2422).*ss(4)+ ...
%         cos(2*pi*(mjd-51544.5)./365.2422).*aa(3) + sin(2*pi*(mjd-51544.5)./365.2422).*aa(4);

vec = sortrows([mjd fit sla],1);
figure(1)
plot(vec(:,1)./365.24+1858.9639,vec(:,2),'k','lineWidth',1.5)
set(gca,'FontSize',15)


figure(2)
plot(vec(:,1)./365.24+1858.9639,vec(:,3)-vec(:,2),'k','lineWidth',1.5)
hold on
plot(vec(:,1)./365.24+1858.9639,movmean(vec(:,3)-vec(:,2),200),'r','lineWidth',1.5)
grid on


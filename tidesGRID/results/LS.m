% Script that reads GESLA data and computes tidal harmonic constants with
% the least-squares method.
% In this script the following functions are used:
% doods.m
% astro.m
% Both the functions are in folder: /win/public/Piccioni/tideGaugeDataNEW/
%
% This script is different from GESLA_LS because we want that the LS are
% computed for the full time series if the records contain < md % of
% missing data (md=missing data).
%
% IMPORTANT: even though the values of amplitude and phase of the
% constituents do not change rapidly through years, it is preferred to
% compute the constants for the longest time series available.
% The problem is that some records contain a large amount of missing
% data, and therefore we can set up a pre-selection, which computes the
% least-squares for the full timeseries when the percentage of missing
% data is lower than a certain threshold, otherwise, we try to find a
% subset of time series with continuous observations.
% The subset must be of minimum one year.
%
% Created and edited by Gaia Piccioni. Last edit: 21.11.2018

clear all
close all

% select the tidal constituents to use in this least squares:
dw ={'M2'; 'K1'; 'N2'; 'O1'; 'P1'; 'Q1'; 'K2'; 'S2'};

% select rhe threshold (in %) below which the full time series is processed
pThreshold = 30; % 25

% read the tidal catalogue according to the tidal constituents chosen,
% in order to get the 6 Doodson numbers:
fcat=readtable('/win/public/Piccioni/tideGaugeDataNEW/catHW95');

doodson = zeros(length(dw),6);
for j=1:length(dw)
    
    istr = find(strcmp(dw{j},table2array(fcat(:,1))));
    
    doodson(j,:) = [fcat.Var2(istr) fcat.Var3(istr) fcat.Var4(istr) ...
        fcat.Var5(istr) fcat.Var6(istr) fcat.Var7(istr)];
    
end


% Path of GESLA dataset
d = dlmread('SLA_324.txt');
lon = d(:,2);
lat = d(:,3);
time = d(:,4); % time in modified julian days
sl = d(:,5);
wgs = d(:,6); % weights according to distance to node

% .................................................................
aaa = sortrows([time sl],1);
figure
plot(aaa(:,1),aaa(:,2),'b')
grid on
axis tight
set(gca,'FontSize',14)
xlabel('Time [MJD]')
ylabel('Sea level [cm]')


% Call Doodson script, given time in mjd and doodson numbers
% if there are more constituents and more observations than 1,
% utheta and f are matrices, with dimension rows=observation,
% columns=number of constituents.
[utheta,f]=astro(dw,doodson,time);

% Set up design matrix D and perform least squares the equation
% to be solved is(p. 268 of book Fu and Cazenave) :
% note that time is expressed in years, starting on 01.01.2000 at 00:00

D = [ones(length(time),1) (time(:)-51544.5)./365.25];

for jjj = 1:length(dw)
    D = [D f(:,jjj).*cos(utheta(:,jjj)) f(:,jjj).*sin(utheta(:,jjj))];
end
x = D\sl;

% compute amplitude (in cm) and phase (in deg) of each constituent
A  = x(3:2:end-1);
B  = x(4:2:end);
am = sqrt(A.^2+B.^2);
ph = atan2d(B,A);

% compute standard deviation of amplitude and phase
sigma0 = sqrt(dot(sl-D*x,sl-D*x)/(length(sl)-length(dw)));
sigmaX = sigma0*diag(inv(D'*D));
sigmaA = sigmaX(3:2:end-1);
sigmaB = sigmaX(4:2:end);

% this formulae are computed with the rules of error
% propagation. The developments can be seen in:
% /win/public/Piccioni/tideGaugeDataNEW/leastSquaresEOT.docx
% the results are the formulae for SIGMA_a and SIGMA_p.
sigAM = sqrt((2.*sigmaA.*A).^2+(2*sigmaB.*B).^2);
sigPH = (180/pi)*(1./(1+(B./A).^2)).*abs(B./A).*sqrt((sigmaA./A).^2+(sigmaB./B).^2); % IN DEGREES

bbb = sortrows([time D*x],1);
figure
plot(aaa(:,1)/365.24,aaa(:,2),'b')
hold on
plot(bbb(:,1)/365.24,bbb(:,2),'r')
grid on
axis tight
set(gca,'FontSize',14)
xlabel('Time [MJD]')
ylabel('Sea level [cm]')



% 
% % save results in a column file
% % the table must have the following information:
% % lon,lat,constituent,amplitude,phase,std(am),std(ph), time of
% % last observation (only year).
% fileID = fopen(['/win/public/Piccioni/tideGaugeDataNEW/GESLA/resultsAuto/round30pc/' char(files(i).name) ],'w');
% %     fileID = fopen(['/win/public/Piccioni/tideGaugeDataNEW/GESLA/resultsAuto/round30pc/' char(fn{1}(i)) ],'w');
% 
% formatSpec = '%8.4f\t%8.4f\t%5s\t%8.3f\t%8.3f\t%8.3f\t%8.3f\t%8.2f\t%10i\t%8.2f\t%15s\t%15s\t%15s\n';
% for row = 1:length(dw)
%     fprintf(fileID,formatSpec,lat,lon,dw{row},...
%         am(row),ph(row),sigAM(row),sigPH(row), pmd, length(time),max(diff(time)),...
%         [num2str(day(datetime(time(1),'ConvertFrom','modifiedjuliandate')),'%02i')     '/'...
%         num2str(month(datetime(time(1),'ConvertFrom','modifiedjuliandate')),'%02i')   '/'...
%         num2str(year(datetime(time(1),'ConvertFrom','modifiedjuliandate')))    ], ...
%         [num2str(day(datetime(time(end),'ConvertFrom','modifiedjuliandate')),'%02i')   '/'...
%         num2str(month(datetime(time(end),'ConvertFrom','modifiedjuliandate')),'%02i') '/'...
%         num2str(year(datetime(time(end),'ConvertFrom','modifiedjuliandate')))  ], ...
%         code);
% end
% 
% fclose(fileID);



% fclose(fileSL);

% % % % % %% Plot the data of the discarded files
% % % % %
% % % % % % fff = fopen('/win/public/Piccioni/tideGaugeDataNEW/GESLA/resultsAuto/discardedFirstRound/disc_missD.txt');
% % % % % % missData = textscan(fff,'%s %f');
% % % % % % ggg = fopen('/win/public/Piccioni/tideGaugeDataNEW/GESLA/resultsAuto/discardedFirstRound/disc_short.txt');
% % % % % % tooShort = textscan(ggg,'%s %f');
% % % % % %
% % % % % fclose(fff) ;
% % % % % fclose(ggg);
% % % % %
% % % % % % plot the % of missing data
% % % % % figure
% % % % % plot(1:length(missData{2}),missData{2})
% % % % % grid on
% % % % % ylabel('Missing data [%]')
% % % % % xlabel('Records discarded')
% % % % % set(gca,'FontSize',15)
% % % % %
% % % % %
% % % % % % plot the length of discarded data
% % % % % figure
% % % % % plot(1:length(tooShort{2}),tooShort{2})
% % % % % grid on
% % % % % ylabel('Record length [days]')
% % % % % xlabel('Records discarded')
% % % % % set(gca,'FontSize',15)
% % % % %
% % % % %
% plot the percentage of missing data for timeseries flagged (and the % is
% the one computed after excluding the timeseries extremes with non-valid
% values).
% fp = fullfile('/win/public/Piccioni/tideGaugeDataNEW/GESLA/resultsAuto/firstRound/','*');
% fl = dir(char(fp));
% formatSpec = '%f %f %s %f %f %f %f %f %s %s %s';
% 
% missData=[];
% stdM2=[];
% amM2=[];
% for sss = 3:length(fl)
%     fff=fopen(['/win/public/Piccioni/tideGaugeDataNEW/GESLA/resultsAuto/firstRound/' char(fl(sss).name)]);
%     tg = textscan(fff,formatSpec);
%     missData = [missData; tg{8}(1)];
%     %     if tg{6}(1) > 100
%     %         disp(fl(sss).name)
%     %     end
%     stdM2 = [stdM2; tg{6}(1)];
%     amM2  = [amM2; tg{4}(1)];
%     fclose(fff);
% end
% 
% figure
% histogram(missData,50)
% grid on
% xlabel('Missing data [%]')
% ylabel('Counts')
% set(gca,'FontSize',15)
% ylim([0 450])
% % % % %
% % % % % % figure
% % % % % % plot(missData,stdM2,'.r')
% % % % % % grid on
% % % % % % xlabel('Missing data [%]')
% % % % % % ylabel('\sigma_{M2} [cm]')
% % % % % % set(gca,'FontSize',15)
% % % % % % ylim([0 1.5])
% % % % % % xlim([0 80])
% % % % % %
% % % % % %
% % % % % %
% % % % % %

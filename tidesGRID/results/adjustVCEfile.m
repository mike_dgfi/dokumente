% script to fill VCE file with missing missions
clear all
close all

% path of results
rp = '/DGFI8/H/work_gaia/tidesGRID/results/EOTsla34_avgSla_VCE_varCap_lat_nSea/';

% VCE file
vce=dlmread([rp 'VCE']);
vceNoCoord=vce(:,3:end);

% observation file (needed because we have to check if the mission was
% included in VCE calculations or there were not enough data:
obs=dlmread([rp 'obsCount']);
obsNoCoord=obs(:,3:end);

% remember that we have stored data with columns: 
% lon,lat,TP,J1,J2,EN,E2,E1c,E1g,TPem,J1em,J2em,J3

% we check line by line what are the missions missing with obs, and we fill
% the vce lines with zeros at the same positions. First, erase zeros from
% vce lines, because function dlmread adds zeros to get regular matrices.
vce_new = zeros(length(obsNoCoord(:,1)),length(obsNoCoord(1,:)));

for i = 1:length(obs(:,1))
    
    i0=vceNoCoord(i,:)~=0;
    vce0=vceNoCoord(i,i0);
    
    zobs = find(obsNoCoord(i,:)~=0);
    count=1;
    for j=1:length(zobs)
        if count<=length(vce0)
            vce_new(i,zobs(j)) = vce0(count);
            count=count+1;
        end
    end
end



% write output file to plot it with GMT
weights=[vce(:,1:2) vce_new];
dlmwrite('VCE_zeroFilled_northSea',weights,'delimiter',' ')

% plot scatter plots to see d2c vs mission weights vs nobs

clear all
close all

% data are columns files with: nobs, miss. weight, d2c
dJ1=dlmread('/DGFI8/H/work_gaia/tidesGRID/results/wObsD2C_J1');
dE2=dlmread('/DGFI8/H/work_gaia/tidesGRID/results/wObsD2C_E2');

ii = find(dJ1(:,3)<0);

dJ1(ii,3)=0;

figure
scatter(dJ1(:,3),dJ1(:,2),30,dJ1(:,1),'fill','MarkerEdgeColor',[0.1 0.1 0.1])
colormap hsv
h=colorbar;
ylabel(h,'nobs')
grid on 
set(gca,'fontSize',15)
xlabel('Distance to coast [km]')
xlim([0 1000])
ylabel('Weights [%]')
caxis([0 1000])
ylim([0 50])

inobs10pc=find(dJ1(:,3)<=200 & dJ1(:,2)>10 & dJ1(:,1)>600);


% figure
% scatter(dJ1(:,3),dE2(:,2),30,dE2(:,1),'fill','MarkerEdgeColor',[0.1 0.1 0.1])
% colormap hsv
% h=colorbar;
% ylabel(h,'nobs')
% grid on 
% set(gca,'fontSize',15)
% xlabel('Distance to coast [km]')
% ylabel('Weights [%]')
% caxis([0 1000])
% xlim([0 1000])
% ylim([0 50])
% 


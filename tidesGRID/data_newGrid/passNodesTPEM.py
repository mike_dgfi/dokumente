import os
from OpenADB.MVA import *

# import info from launchPYandEXE in /DGFI93/home/piccioni/Dokumente/tidesGRID
#filein=os.environ["filein"]
filein="/DGFI8/H/work_gaia/tidesGRID/data_newGrid/nodes_grid_World.txt"
fin = open(filein,'r')

overwrite=0
mission = 'topex_em'
pout='/DGFI8/H/work_gaia/tidesGRID/data_newGrid/TPEMnodes'

if not os.path.exists(pout):
    os.mkdir(pout)
    
for line in fin:
    
    coords = line.split()
    cnt    = np.float(coords[0])
    clon   = np.float(coords[1])
    clat   = np.float(coords[2])
    dpt    = np.float(coords[3])    
    
    count = '%08d' % cnt
    fout    = pout+'/'+count+'_TPEM.bin'
    
    if overwrite==0 and os.path.isfile(fout):
		print fout,'already exist!'
		continue
    
    outfile = open(fout,'wb') 

    radius=200  # in km

    idx_data = get_index_by_circle(mission,clon,clat,radius)
    data = {}
    cycles = idx_data.keys()
    cycles.sort()

    for cycle in cycles:						
	    if not data.has_key(cycle):
		    data[cycle] = {}
	    passes = idx_data[cycle].keys()
	    passes.sort()
	    for passnr in passes:
		    data[cycle][passnr] = idx_data[cycle][passnr].keys()
		    idxs = idx_data[cycle][passnr].keys()
		    idxs.sort()	
		    outfile.write(struct.pack('I',int(cycle)))
		    outfile.write(struct.pack('I',int(passnr)))
		    outfile.write(struct.pack('I',idxs[0]))
		    outfile.write(struct.pack('I',idxs[-1]))
        
    outfile.close()
    
fin.close()
 

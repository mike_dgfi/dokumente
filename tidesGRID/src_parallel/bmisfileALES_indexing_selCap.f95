   program bmisfileALES_indexing
   !
   ! This routine reads in the information of nodes (node ID, lat,lon, path of 
   ! the node files) and writes files for each mission with data within a 
   ! certain cap-size (input variable=csize) 
   ! ---------------------------------------------------------------------------	  
   ! --------------------------------------------- PROCESS TOPEX-TYPE SATELLITES
   ! ---------------------------------------------------------------------------
      IMPLICIT NONE
      character(LEN=30)              :: pmva
      character(LEN=8)               :: nodenr
      character(LEN=120)             :: nodepath,iorb,eot,slf,dtu,itm,otd,mss
      character(LEN=120)             :: wtr,dtr,ion,ssb,tep,ivb,oer,otd17,otd15
      character(LEN=120)             :: nodefile,omk,dtc,outname,s32
      character*12,dimension(11)     :: ms   
      character*30,dimension(11)     :: prefix,prefnew,ndnew
      character(LEN=3)               :: cy,misref
      character(LEN=4)               :: ps
      character(LEN=120)             :: filename,pOut  
      character*12,dimension(11)     :: mnodes,misfold   
      integer*4                      :: id,lon,lat,isec,msec,mssh,mssh4
      integer*4                      :: glon,glat,hsat,ralt,coast
      integer*4                      :: recstart,recstop,dpt,j
      integer                        :: rnd,k,nf,onull,sz,szo,iosor,iosmk
      integer                        :: iosmis,r,q,mm,trec,sorb,dl1,dl2,dl3
      integer                        :: iosnode,tnull,lnull,recnum,iossl
      integer                        :: iosi,iosfes,ioseot,iost,cend,ndrec
      integer                        :: ioswt,iosdt,iosio,iosib,ioste,iosssb
      integer                        :: vrec,zz,ddd,oldpass,iosoe,ios,rJ3
      integer                        :: rTP,rJ1,rJ2,rEN,rE2,rE1c,rE1g,rTPem,rJ1em,rJ2em
      integer                        :: iosv,iosvv,cobs,iosdc,iosh,countSWH
      integer*2                      :: wtrop,dtrop,ionos,errorb,otide,ltide
      integer*2                      :: otide17,ltide17,otide15,ltide15
      integer*2                      :: pass,startrr,endrr,cc 
      integer*2                      :: etide,ptide,invb,emb,slaint
      integer*1                      :: oflags,gflags,iflags,ws,msk
      integer*2                      :: sla31,st,sw,ssw,sig0
      real                           :: start1,finish1,start2,finish2,start3,finish3
      real                           :: start4,finish4,start5,finish5,start6,finish6
      real                           :: start7,finish7      
      logical                        :: fe,iex,tex,lex,ionex,msex,slex,exfes,dcex
      double precision               :: jd85,mjd85,tm,cradsq,dx,dy,dist,sla
      double precision               :: dum,slh,dum3,erad,tmav,slaav,wgav
      double precision               :: avTP,avJ1,avJ2,avEN,avE2,avE1c,avE1g
      double precision               :: stTP,stJ1,stJ2,stEN,stE2,stE1c,stE1g
      double precision               :: stSWH,avSWH,sumSWH,sumSWHsq
      double precision               :: minSWH,maxSWH,slawg,sumwg,maxWG,maxTM
      double precision               :: pi,d2r,sigma,wg,ar,csize,ttt,dlon,dlat 
      double precision,dimension(11) :: avg,slasum,slasqr,stdsla,sumsl,sumslsq
      integer, dimension(11)         :: recn,nr
      
!       print*,'here'

      read(*,*) csize
!       print*, csize
      read(*,'(a)') nodepath 
!       print*,nodepath
      read(*,*) ndrec
!       print*,ndrec
      read(*,*) mm
!       print*,mm
      read(*,'(a)') filename
!       print*,filename
      read(*,'(a)') pOut
!       print*,pOut
!        stop
       
      pmva='/DGFI8/A/MVA/'
    
      cy='ccc'
      
      ! 20Hz-frequency data
!       ms(1)='jason1_hf'
!       ms(2)='jason2_hf'
!       ms(3)='envisat_hf'
!       ms(4)='ers2_hf'

      ! 1Hz-frequency data
      ms(1)='topex'
      ms(2)='jason1'
      ms(3)='jason2'
      ms(4)='envisat_v3'
      ms(5)='ers2'
      ms(6)='ers1c'
      ms(7)='ers1g'
      ms(8)='topex_em'
      ms(9)='jason1_em'
      ms(10)='jason2_em'
      ms(11)='jason3'    
      
      prefix(1)='d2p_tp_'
      prefix(2)='d2p_j1_'
      prefix(3)='d2p_j2_'
      prefix(4)='d2p_en_'
      prefix(5)='d2p_e2_'
      prefix(6)='d2p_e1c_'
      prefix(7)='d2p_e1g_'
      prefix(8)='d2p_tpem_'
      prefix(9)='d2p_j1em_'
      prefix(10)='d2p_j2em_'
      prefix(11)='d2p_j3_'
      
      misfold(1)='TPnodes'
      misfold(2)='J1nodes'
      misfold(3)='J2nodes'
      misfold(4)='EN_V3nodes'
      misfold(5)='E2nodes'
      misfold(6)='E1Cnodes'
      misfold(7)='E1Gnodes'
      misfold(8)='TPEMnodes'
      misfold(9)='J1EMnodes'
      misfold(10)='J2EMnodes'
      misfold(11)='J3nodes'
      
      mnodes(1)='TP.bin'
      mnodes(2)='J1.bin'
      mnodes(3)='J2.bin'
      mnodes(4)='EN.bin'
      mnodes(5)='E2.bin'
      mnodes(6)='E1c.bin'
      mnodes(7)='E1g.bin'
      mnodes(8)='TPEM.bin'
      mnodes(9)='J1EM.bin'
      mnodes(10)='J2EM.bin'
      mnodes(11)='J3.bin' 
      
      ! ------------------- define PI, deg-to-rad conversion, and time from 1985
      pi    = 3.141592654d0
      d2r   = pi/180.
      erad  = 6371.
      jd85  = 2446066.5d0
      mjd85 = jd85-2400000.5d0     
      
     
      ! ------------------------------------------------------------------------	  
      ! --------------------------------------- START PROCESS MISSION-BY-MISSION
      ! ------------------------------------------------------------------------
      recn(1:11)=0   ! initialize count of record for each node for each mission
!       open(900,file='recnumMission.txt',access='append')
      
      write(misref(1:3),'(I3.3)') mm
      open (800+mm,file=trim(pOut)//'/cnt_'//trim(misref)) 
!       sumsl(1:11)=0
!       sumslsq(1:11)=0
!       sumSWH=0
!       sumSWHsq=0
!       minSWH=3   ! initial value very large
!       maxSWH=-3  ! initial value very small
!       countSWH=0
    
        open (16,file=filename,access='direct',recl=16, &
        form='unformatted',status='OLD',iostat=ios)
        read (16,rec=ndrec) id,lon,lat,dpt 
        
!     ! --------------- define cap size according to user request. if cap size=0, then
      ! --------------- the user wants a variable cap size according to the depth node. 
      if (csize .eq. 0) then 
!        change cap size according to ocean depth
!         if(dpt .le. 400) then
!             csize=0.575*dpt+100
!             
!         else
!             csize=330.d0
!         endif
! 
!        change cap size according to latitude
        if(abs(lat) .gt. 0) then
            csize=330-3*abs(lat)            
        else
            csize=330.d0
        endif
      endif
      
      cradsq=csize*csize/4   ! this must be the radius (half csize)
      ttt=csize*0.4d0/2
      sigma=log(2.d0)/(ttt*ttt)	
  
  
        
!         do mm=1,11 !-----IMPORTANT: REMEMBER TO CHANGE THIS WHEN YOU USE ALONG-TRACK DATA
!         print*,'start mission'
!         call cpu_time(start1)  ! measure time each mission
        
        vrec=1
        
        ! create the file where to save the data for each mission for each node
        write(nodenr(1:8),'(I8.8)') id
        outname = trim(prefix(mm))//nodenr(1:8)
        nf=80+mm
        open (nf,file=outname,access='direct',recl=24,&
            form='unformatted',status='REPLACE',iostat=iosmis)	
        
        ! read the file of the mission where all the passes for each node are stored
        write(nodenr(1:8),'(I8.8)') id
        nodefile=trim(nodepath)//'/'//trim(misfold(mm))//'/'// & 
                nodenr//'_'//trim(mnodes(mm))
!         print*,nodefile
        
        INQUIRE(file=nodefile,exist=fe,SIZE=sz)
        rnd=sz/16 
        
!         print*,rnd
        
        if (fe) then ! if there are tracks for this node (if file exists)  
	  
          open (111,file=nodefile,access='direct',recl=8, &
          form='unformatted',status='OLD',iostat=iosnode)  

          do k=1,rnd  ! go through all passes of interest for the node
          
!              call cpu_time(start2)  ! measure time each pass
                
            read(111,rec=k) pass,cc,startrr,endrr
              
!                 call cpu_time(start3)   ! measure time each cycle
    
            write(cy(1:3),'(I3.3)') cc
                
            if (mm .le. 3 .or. mm .ge. 8) then
                ps='ppp'
                write(ps(1:3),'(I3.3)') pass
            elseif (mm .gt. 3 .and. mm .le. 7) then
                ps='pppp'
                write(ps(1:4),'(I4.4)') pass             
            endif   

            ! ----------------------------------- open binary files from MVA		
            ! ------------------------------ check files exist and not empty
            ! --------------------------------------------------- orbit file
            if (mm .eq. 2 .or. mm .eq. 9 .or. mm .eq. 10 .or. & 
                mm .eq. 11 .or. mm .eq. 4) then
                iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
                        '/'//cy//'_'//trim(ps)//'orbit.00'
            elseif (mm .eq. 3) then
                iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
                        '/'//cy//'_'//trim(ps)//'orbit.30'
            elseif (mm .eq. 1 .or. mm .eq. 5 .or. mm .eq. 6 .or. &
                    mm .eq. 7 .or. mm .eq. 8) then
                iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
                        '/'//cy//'_'//trim(ps)//'orbit.24'
            endif
            !        Old envisat     elseif (mm .eq. 4) then
!                 iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
!                         '/'//cy//'_'//trim(ps)//'orbit.06' 
            
            INQUIRE(file=iorb,exist=iex,size=sorb)
                
            ! --------------------------------------------------  instr file
            itm = trim(pmva)//trim(ms(mm))//'/'//cy// &
                '/'//cy//'_'//trim(ps)//'instr.00'	        
            INQUIRE(file=itm,exist=tex)

            ! ------------------------------------------------------ FES2014
!             otd17 = trim(pmva)//trim(ms(mm))//'/'//cy// &
!                     '/'//cy//'_'//trim(ps)//'tideo.17'    
!             INQUIRE(file=otd17,exist=exfes)
! !             ! ------------------------------------------------------- EOT11a
!             otd15 = trim(pmva)//trim(ms(mm))//'/'//cy// &
!                     '/'//cy//'_'//trim(ps)//'tideo.15'    

                                                
            ! ---------------------------------------- Distance to coast .00
            dtc = trim(pmva)//trim(ms(mm))//'/'//cy// &
            '/'//cy//'_'//trim(ps)//'distcoast.00'
            INQUIRE(file=dtc,exist=dcex)
            
            ! ------------------------------------------------------ SLA .34
            s32 = trim(pmva)//trim(ms(mm))//'/'//cy// &
            '/'//cy//'_'//trim(ps)//'slafg.34'
            INQUIRE(file=s32,exist=slex)

            ! -------------------------------------- If files exist, open...
            if (iex .and. tex .and. slex .and. dcex & 
                    .and. sorb .gt. 100) then		    
!                 .and. ionex .and. msex .and. exfes  
                
                
                open (30,file=iorb,access='direct',recl=13, &
                form='unformatted',status='OLD',iostat=iosor)   
                
                open (40, file=itm,access='direct',recl=22, &
                    form='unformatted',status='OLD',iostat=iost)
                    
                open (41, file=otd17,access='direct',recl=4, &
                    form='unformatted',status='OLD',iostat=iosfes)
                    
                open (42, file=otd15,access='direct',recl=4, &
                    form='unformatted',status='OLD',iostat=ioseot)
            
                open (78, file=dtc,access='direct',recl=4, &
                    form='unformatted',status='OLD',iostat=iosdc)	

                open (98, file=s32,access='direct',recl=3, &
                    form='unformatted',status='OLD',iostat=iossl)
                    
                ! ------- ... and read the records indicated in the nodefile
                trec=sorb/13
                
                slawg = 0
                sumwg = 0
                maxWG = 0

               
                do j=startrr,endrr    !1,trec
        
!                     print*,j
                    ! ----- read file and check what are the data to account 

                    read(30,rec=j) glon,glat,hsat,oflags
                    
                    ! ------------------------------------- compute distance 
                    ! we basically apply the Pythagorean theorem
                    ! on a sphere projected on a plane (accounting for the 
                    ! different distance between meridians according to latitude)
                    ! account for the extreme longitudes: take the smallest
                    ! distance between the node and the track point in longitude. 
                    if (glat .le. lat+4000000 .and. glat .ge. lat-4000000) then
                            
!                             call cpu_time(start4)   ! measure time spent to compute distance
                        
                        dl1 = abs(lon - glon)
                        dl2 = abs(lon - glon + 360000000)
                        dl3 = abs(lon - glon - 360000000)
                    
                        dlon = min(dl1,dl2,dl3)*d2r/1000000.
                        dlat = abs(lat - glat)*d2r/1000000.     
                        dx   = dlon*erad*cos(glat*d2r/1000000.)
                        dy   = dlat*erad
                        dist = dx*dx+dy*dy     ! N.B. already square value
                    
!                             call cpu_time(finish4)
!                             print '("distance = ",f6.3," seconds.")',finish4-start4
                        
                        read(40,rec=j) isec,msec,ralt,st,sw,ssw,sig0,ws,iflags
!                         read(48,rec=j) mssh    
!                         read(41,rec=j) otide17,ltide17
!                         read(42,rec=j) otide15,ltide15
                        read(98,rec=j) slaint,gflags                            
                        read(78,rec=j) coast
                            
!                             call cpu_time(start5)   ! measure time spent to compute SLA
                        
                        sla = dble(slaint)
!                         +otide15+ltide15-otide17-ltide17

!                         
!   		  CHECK POINT
!           		        if(dist .le. cradsq) then
!                           print*, dist,sla,hsat,ralt,emb,wtrop, &
!         			              dtrop,ionos,ptide,invb,etide,mssh,otide,ltide
!         		        endif
!             END OF CHECK POINT

            ! 			! ----- if SLA are reasonable and within cap size, go on
                        if (sla .ge. -2500 .and. sla .le. 2500 .and. & 
                            dist .le. cradsq .and. gflags .eq. 0 .and. &
                            oflags .le. 16 .and. iflags .le. 4 .and. &
                            coast .gt. 6000) then
!   .and. iflags .eq. 0
!                             
!                            .and. &
!                                   print*,'yes'  

!                                 call cpu_time(start6) ! time to compute weights
                            
                            wg   = exp(-sigma*dist)
                            
!                                 call cpu_time(finish6)
!                                 print '("weight = ",f6.3," seconds.")',finish6-start6  
                            
                            ! ----------------------------- compute time in JDAY
                            tm=(dble(isec)+(dble(msec)/1000000.))/ &
                                86400.d0+ mjd85	
                            
                            ! ----- write to file each accepted SLA in one track
!                             write(nf,rec=vrec) tm,sla,wg
!                             
!                             print*, id,glon/1000000.,glat/1000000.,sla,wg,tm,& 
!                                     sqrt(dist),mm,pass,errorb
!                             
!                             vrec=vrec+1 
                            
!                             recn(mm)=recn(mm)+1
!                             sumsl(mm)=sumsl(mm)+sla
!                             sumslsq(mm) = sumslsq(mm)+sla**2
                            
                            ! ----------- compute weighted sum of SLA and sum of
                            ! ------------- weights to get weighted-averaged SLA
                            
!                                 call cpu_time(start7) ! time to compute weighted SLA
!                             print*, glon,glat,sla,wg,sqrt(dist)
                            slawg = slawg + sla*wg
                            sumwg = sumwg + wg
                            
                            ! ------ find max weight (and therefore min distance
                            ! ------ from node) and corresponding time to use it 
                            ! -------- for the corresponding final weighted SLA.
                            ! ---------- See EOT11a report page 8, section 2.2.4
                            if (wg .gt. maxWG) then
                                maxWG=wg
                                maxTM=tm
                            endif  
                            
!                                 print*,wg,maxWG
!                                 call cpu_time(finish7)
!                                 print '("w-av SLA = ",f6.3," seconds.")',finish7-start7  

                            ! compute minmax SWH and elements for average and std
                            ! NOTE that SWH is already in cm!!!!!!!!!!!!!!!!!!!!
                ! 			  if (sw .gt. -50 .and. sw .lt. 1150) then
                ! 			  
                ! 			      sumSWH=sumSWH + sw
                ! 			      sumSWHsq=sumSWHsq + sw**2
                ! 			      countSWH = countSWH + 1
                ! 			      
                ! 			      if (sw .gt. maxSWH) then
                ! 				maxSWH=sw
                ! 			      endif			  
                ! 			      if (sw .lt. minSWH) then
                ! 				minSWH=sw
                ! 			      endif
                ! ! 			      print*,countSWH,sumSWH,sumSWHsq 		  
                ! 			  endif
                            
                        endif ! end condition to accept SLA   
                    endif     ! end latitude bound condition
                end do    ! finish one track (i.e. one line of nodefile) 
                
                ! --------- write to file weighted-averaged SLA of one track
                !
                !
                ! ............. ATTENTION!! SLA is saved in cm!!! ..........
                ! 
                ! 
        
                if (maxWG .ne. 0) then
!                         print*,maxTM,slawg/sumwg,maxWG    
                    write(nf,rec=vrec) maxTM,slawg/(sumwg*10),maxWG    
                    vrec=vrec+1 
                    recn(mm)=recn(mm)+1
!                         print*, maxTM,slawg/(sumwg*10),maxWG  
                endif
                

                
                
                close(30)    
                close(40)
!                 close(41)
!                 close(42)
!                 close(78)
                close(98)
                    
                    
                endif ! end condition if track file exists  
                
!                    call cpu_time(finish3)
!                    print '("1-cycle loop = ",f6.3," seconds.")',finish3-start3   
!                      print*,'cycle',cc
        

!           call cpu_time(finish2)
!           print '("1-pass loop = ",f6.3," seconds.")',finish2-start2
          end do         ! end loop through pass
          
        endif            ! end condition if pass file exists          

        close(nf)
        
!         call cpu_time(finish1)
!         print '("1-mission loop = ",f6.3," seconds.")',finish1-start1     
!        print*,'end mission'
!       end do   ! finish one mission
      
      ! ------------------------------------------------------------------------	  
      ! ------------------------------------------------------------ END PROCESS
      ! ------------------------------------------------------------------------   
      
      ! ----------------- SAVE COUNT OF OBSERVATION FOR EVERY MISSION AT EACH NODE 
      write(800+mm,*) lon/1000000.,lat/1000000.,recn(mm)
!       write(900,*) recn(mm)
      
      close(800+mm)
!       close(900)
      ! mean and std in cm (i.e. 0.1*)
!       avTP = 0.1*sumsl(1)/rTP
!       avJ1 = 0.1*sumsl(2)/rJ1
!       avJ2 = 0.1*sumsl(3)/rJ2 
!       avEN = 0.1*sumsl(4)/rEN
!       avE2 = 0.1*sumsl(5)/rE2 
!       avE1c= 0.1*sumsl(6)/rE1c
!       avE1g= 0.1*sumsl(7)/rE1g
!           
!       stTP = 0.1*sqrt(sumslsq(1)/rTP-avTP*avTP)    
!       stJ1 = 0.1*sqrt(sumslsq(2)/rJ1-avJ1*avJ1)
!       stJ2 = 0.1*sqrt(sumslsq(3)/rJ2-avJ2*avJ2) 
!       stEN = 0.1*sqrt(sumslsq(4)/rEN-avEN*avEN) 
!       stE2 = 0.1*sqrt(sumslsq(5)/rE2-avE2*avE2)
!       stE1c= 0.1*sqrt(sumslsq(6)/rE1c-avE1c*avE1c)
!       stE1g= 0.1*sqrt(sumslsq(7)/rE1g-avE1g*avE1g)
      
      ! do the same for the SWH values
      ! NOTE that SWH is already in cm!!!!!!!!!!!!!!!!!!!!
!       avSWH = sumSWH/countSWH
!       stSWH = sqrt(sumSWHsq/countSWH-avSWH*avSWH) 
!       print*,avSWH,sumSWH,countSWH,sumSWHsq,sumSWHsq/countSWH,avSWH*avSWH,stSWH

   end program
   

 program computeTIDES
 ! -----------------------------------------------------------------------------
 ! this program reads the node files and retrieves all the data 
 ! (through all missions) needed to compute the least squares for one node
 ! Gaia Piccioni Sep 2018
 ! -----------------------------------------------------------------------------
    implicit none
    integer                            :: i,nodenumber,sz,szn,szo,ios,recnum,r,q
    integer                            :: lnp,iosnode,iosor,k,cc,j,lnm,indj,cbeg
    integer                            :: sztm,iost,rnd,countobs,iosh,kk,zz,derr
    integer                            :: cend,cobs,rnde,iii,aa,bb,jjj,mcnt,mk
    integer                            :: iose2,iosen,iosj1,iosj2,iostp,mkk,kkk
    integer                            :: d1,d2,d3,d4,d5,d6,gg,tdsize,cntwg,szen
    integer                            :: infosvd,lwork,sx,si,vrec,iosv,iosvv,jk
    integer                            :: nrec,slnm,mm,mn,nobs,sn,cntsum,vv,jj
    integer*4                          :: glon,glat,hsat,id,lon,lat,isec,msec,dpt
    integer*4                          :: ccold
    integer*4                          :: ind,miss,dcc
    integer                            :: tdnum,ddd,missn,mend,rTPem,rJ1em,rJ2em
    integer                            :: rnum
    integer,allocatable                :: snull(:),cnt(:),whichm(:)
    integer,allocatable                :: dood(:,:)
    logical                            :: ff
    character(LEN=30)                  :: pmva,nodeo
    character(LEN=8)                   :: nodenr
    character(LEN=6)                   :: dw
    character(LEN=30)                  :: nodetp,nodej1,nodej2,nodeen,nodee2
    character(LEN=120)                 :: filename,nodepath,pOut
    character*3,allocatable            :: darw(:)
    character*30,dimension(11)         :: prefnew,ndnew
    integer,dimension(11)              :: im
    real                               :: start,finish
    real*8                             :: fr,ttt,arav,depth
    real*8                             :: pi,d2r,sigma,dum4,ar,CIRCLE
    real, allocatable                  :: wgv(:)
    double precision                   :: tm,dum,f,u,tmv,err,tmav
    double precision                   :: s,h,p,N,pp,gmst,tau,Tu0,gmst0,rr,xMpar
    double precision                   :: rmod,t,mjd,am,ph,c0,s0,c1,s1,slaav
    double precision                   :: wg,sla,TT,xi
    double precision                   :: stTP,stJ1,stJ2,stEN,stE2,stE1c,stE1g
    double precision                   :: avTP,avJ1,avJ2,avEN,avE2,avE1c,avE1g
    double precision                   :: u0,detinv,sum1,sum2,sum3,iang,inc,nu
    double precision                   :: tmp1,tmp2,tmp3,sumvar
    double precision                   :: xNm,errv,l,omega,ll,Fa,D,wgav
    double precision                   :: slasum,slasqr,slh,dum3,stdsla,avg
    double precision                   :: sam,sph,xAtWAx,slWAx,sigma0
    double precision                   :: errsum,ntrsum,xNlx,xNr
    double precision                   :: avSWH,stSWH,minSWH,maxSWH
    double precision, allocatable      :: Npar(:,:,:),Mpar(:,:),xNl(:) 
    double precision, allocatable      :: Nl(:,:),Nl2(:,:),Nr(:),ntr(:,:),Nlx(:)
    double precision, allocatable      :: A(:,:),x(:),Nm(:),W(:),Nls(:,:)
    double precision, allocatable      :: iAtWA(:,:),AtW(:,:),UUSD(:,:),Ax(:)
    double precision, allocatable      :: slv(:),UUSS(:,:),sigmaX(:)
    double precision, allocatable      :: VT(:,:),UU(:,:),SDG(:,:),SM(:,:)
    double precision, allocatable      :: SS(:),worksvd(:),SVD(:,:),xAtWA(:)
    double precision, allocatable      :: slWtp(:),slWj1(:),slWj2(:),slWen(:)
    double precision, allocatable      :: slW(:),rs(:),sr(:),red(:),omg(:)
    double precision, allocatable      :: Nli(:,:),slWe2(:),Aen(:,:),var(:)
    double precision, allocatable      :: Atp(:,:),Aj1(:,:),Aj2(:,:),Ae2(:,:)
    double precision, allocatable      :: sltp(:),slj1(:),slj2(:),slen(:)
    double precision, allocatable      :: wtp(:),wj1(:),wj2(:),wen(:),we2(:)
    double precision, allocatable      :: Axl(:),sle2(:),AxlW(:),slWsl(:)
    

    !-------------------------------------- define file paths, and mission names
!   i=node, filename=node file in binary, pOut=path of output
    read(*,*) i
    read(*,'(a)') filename
    read(*,'(a)') pOut

    
    
    pmva='/DGFI8/A/MVA/' 
    
    prefnew(1)='d2p_tp_'
    prefnew(2)='d2p_j1_'
    prefnew(3)='d2p_j2_'
    prefnew(4)='d2p_en_'
    prefnew(5)='d2p_e2_'
    prefnew(6)='d2p_e1c_'
    prefnew(7)='d2p_e1g_'
    prefnew(8)='d2p_tpem_'
    prefnew(9)='d2p_j1em_'
    prefnew(10)='d2p_j2em_'
    prefnew(11)='d2p_j3_'

    !----------- define PI, deg-to-rad conversion, and time from 1985 conversion
    pi     = 3.141592654d0
    d2r    = pi/180.d0
    
    ! --------------------------------------------------- define Doodson Numbers
    open(23, file='catHW95.dat')
    read(23,*)
    read(23,*)
    
    ! --------------------------- CHOOSE HOW MANY CONSTITUENTS MUST BE PROCESSED
    ! less constituents than 13 is possible, but the order is fixed. Follow always this 
    ! order: M2,N2,S2,K2,2N2,S1,K1,P1,O1,Q1,M4,MM,MF, or modify order in the catalog.
    
    tdnum = 13 ! M2,N2,S2,K2,2N2,S1,K1,P1,O1,Q1,M4,MM,MF
            
    ! ---------------------- READ CATALOG TO EXTRACT INFO ABOUT EACH CONSTITUENT        
    allocate(dood(tdnum,6),darw(tdnum))
    do q=1,tdnum   
       read(23,*,end=9001) dw,d1,d2,d3,d4,d5,d6,fr,c0,s0,c1,s1
       darw(q)   = dw
       dood(q,1) = d1
       dood(q,2) = d2
       dood(q,3) = d3
       dood(q,4) = d4
       dood(q,5) = d5
       dood(q,6) = d6

    end do    
9001 continue  

    ! ---------- Open files needed to save the Constituent parameters at the end   
    open(300,file=trim(pOut)//'/M2',access='append') 
    open(301,file=trim(pOut)//'/S2',access='append')
    open(302,file=trim(pOut)//'/N2',access='append')
    open(303,file=trim(pOut)//'/O1',access='append')
    open(304,file=trim(pOut)//'/K1',access='append')
    open(305,file=trim(pOut)//'/Q1',access='append')
    open(306,file=trim(pOut)//'/P1',access='append')
    open(307,file=trim(pOut)//'/K2',access='append') 
    open(308,file=trim(pOut)//'/MM',access='append')
    open(309,file=trim(pOut)//'/M4',access='append')
    open(310,file=trim(pOut)//'/S1',access='append')
    open(311,file=trim(pOut)//'/2N2',access='append')
    open(312,file=trim(pOut)//'/MF',access='append')
    open(313,file=trim(pOut)//'/SA',access='append')
    open(314,file=trim(pOut)//'/SSA',access='append')

    ! ------------------------------------------ open list of nodes' coordinates
    open (16,file=filename,access='direct',recl=16, &
	 form='unformatted',status='OLD',iostat=ios)
	 
    ! -------- open file with count of observation for each mission at each node 
!     open (888,file=trim(pOut)//'/obsCount',access='append') 
    
    ! ----------- open file with SWH statistics for each node, but both missions 
!     open (999,file=trim(pOut)//'/SWHstatsALES_p44',access='append') 
    
    ! ------------------------------------ open file to save sigma0 with lon,lat
    open (899,file=trim(pOut)//'/sigma0',access='append') 

    ! ------------------------ open file to save weight of missions with lon,lat
    open (599,file=trim(pOut)//'/VCE',access='append')     

    ! ------------ open file to save constant sea level (intercept) with lon,lat
    open (499,file=trim(pOut)//'/off',access='append') 
    
    ! ----------------------- open file to save the sea level trend with lon,lat
    open (399,file=trim(pOut)//'/slaTrend',access='append') 

    
    INQUIRE(FILE=filename, SIZE=szn)
    nodenumber=szn/16
    

	   read (16,rec=i) id,lon,lat,dpt
! 	   print*,id,lon,lat,dpt

        
    ! ----------------------- Subroutine bmisfile extracts data for each mission
    ! ---------------------- within node's cap-size csize and computes files for 
    ! ----------------------- weights. Results are saved in  for single missions

   
! 	if (rnum .gt. 500) then
   
                     
    ! ---------------------- SAVE MEAN, STD, MIN, MAX of SIGNIFICANT WAVE HEIGHT
        ! NOTE that SWH is already in cm!!!!!!!!!!!!!!!!!!!!
!         write(999,*) lon/1000000.,lat/1000000.,avSWH,stSWH,minSWH,maxSWH
   
! 	endif
   ! ---------------------------------------------------------------------------   
   ! ---------------------------------------------------------------------------
   ! ------------------------------------- HERE WE BEGIN OPERATIONS FOR ONE NODE
   ! ---------------------------------------------------------------------------
   ! ---------------------------------------------------------------------------
   
   ! --------------------------------------------------------- create file names 
        write(nodenr(1:8),'(I8.8)') id
    	ndnew(1)=trim(prefnew(1))//nodenr(1:8)
    	ndnew(2)=trim(prefnew(2))//nodenr(1:8)
    	ndnew(3)=trim(prefnew(3))//nodenr(1:8)
    	ndnew(4)=trim(prefnew(4))//nodenr(1:8)
    	ndnew(5)=trim(prefnew(5))//nodenr(1:8)
    	ndnew(6)=trim(prefnew(6))//nodenr(1:8)
    	ndnew(7)=trim(prefnew(7))//nodenr(1:8)
    	ndnew(8)=trim(prefnew(8))//nodenr(1:8)
    	ndnew(9)=trim(prefnew(9))//nodenr(1:8)
    	ndnew(10)=trim(prefnew(10))//nodenr(1:8)
    	ndnew(11)=trim(prefnew(11))//nodenr(1:8)
    	
!     	print*,ndnew(1),ndnew(2),ndnew(3),ndnew(4)   	       
   ! ----------------------- matrix dimensions depend on the number of missions:
   ! ------------------------------------ check how many missions to account for   
   ! ---------------------- (criterium: missions with more than 50 observations)
    	missn=0   
    	im(1:11)=0
        do ddd=1,11         
          INQUIRE(file=ndnew(ddd),exist=ff)
          INQUIRE(FILE=trim(ndnew(ddd)), SIZE=sz)
          
          if (ff .and. sz/24 .gt. 50) then
	        missn=missn+1 
	        im(ddd)=ddd
          endif
          
        end do                     
    
        zz=1
        allocate(whichm(missn))
        do ddd=1,11             
            if (im(ddd) .ne. 0) then
               whichm(zz)=im(ddd)
               zz=zz+1
            endif          
        enddo           
!         print*,missn
        if (missn .ne. 0) then
	
          ! ----------------------------- define the other size of design matrix
	  tdsize=missn+5+2*tdnum        
	  
	  ! ------------------------ allocate elements and build normal matrices
	  allocate(Npar(tdsize,tdsize,missn),Mpar(tdsize,missn),slWsl(missn))
	  allocate(snull(missn),cnt(missn))
	  
	  mn=90
	  do mcnt=1,missn ! for each mission		
          
	      INQUIRE(FILE=trim(ndnew(whichm(mcnt))), SIZE=snull(mcnt))
	      mn=mn+mcnt
    
	      open (mn,file=trim(ndnew(whichm(mcnt))),access='direct',recl=24,&
		  form='unformatted',status='OLD',iostat=iostp) 

	      cnt(mcnt)=snull(mcnt)/24  ! know how many observations we have
	    
	    ! --------------------- Here we build design and weight matrices A,W	            
	      allocate(slv(cnt(mcnt)),W(cnt(mcnt)), A(cnt(mcnt),tdsize), &
		      slW(cnt(mcnt)))	
		      
	      A(1:cnt(mcnt),1:tdsize)=0.
	      
	      do zz=1,cnt(mcnt)	   
            read(mn,rec=zz) mjd,slv(zz),W(zz)
            
            slW(zz)= slv(zz)*W(zz)
            
            ! first columns with drift, annual signal, semi-annual signal
            A(zz,1)=(mjd-51544.5d0)/365.2422d0             ! JD2000 in years
            A(zz,2)=cos(2*pi*(mjd-51544.5d0)/365.2422d0)   ! annual signal
            A(zz,3)=sin(2*pi*(mjd-51544.5d0)/365.2422d0)   ! annual signal
            A(zz,4)=cos(4*pi*(mjd-51544.5d0)/365.2422d0)   ! semi-annual signal
            A(zz,5)=sin(4*pi*(mjd-51544.5d0)/365.2422d0)   ! semi-annual signal
            
            ! note about the mission offset: we call them mission offset because
            ! they were called like this for EOT11a. What we are estimating here
            ! is simply the intercept of the timeseries of each mission, which
            ! is, by definition in Pugh and Woodworth 2017 (Sea level science book
            ! ch. 4) the constant sea level (I can't use the word Mean Sea Level
            ! here because it is misleading) that is seen for a given mission.
            A(zz,2*tdnum+5+mcnt)=1.d0   ! mission offset sub-matrix (last columns)
            
            do kk=1,tdnum 
            
            ! ------ N.B. DWcorr uses subroutine doodsonArgs to get astronomical
            ! --------- arguments, then computes nodal factors f,u for amplitude 
            ! --------- & phase. The output u and f include D-W correction (chi)	      
                call DWcorr(darw(kk),dood(kk,1),dood(kk,2),dood(kk,3), &
                dood(kk,4),dood(kk,5),dood(kk,6),mjd,f,u)
                
            ! -- write the results from the 6th+7th columns of the design matrix
                A(zz,4+kk*2)=f*cos(u)     ! see Fu & Cazenave for short           
                A(zz,5+kk*2)=f*sin(u)     ! explanation cos&sin argument (p. 68)     
                
            end do	  
	      end do 
	      
	    ! --------------------------------------- close file of observations	      
	      close(mn,status='delete') !
	    ! ------------------------------------------------------------------

	      slWsl(mcnt) = dot_product(slW,slv)
      	
	      ! N.B. Here instead of computing the transpose At we just use
	      ! the indices of matrix A with inverted order, i.e.:
	      ! instead of doing: At(aa,bb)*W(bb) we do: A(bb,aa)*W(bb)
	      ! in this way we avoid to allocate more memory.
	      allocate(AtW(tdsize,cnt(mcnt)))
	      do aa=1,tdsize
		do bb = 1,cnt(mcnt)
		    AtW(aa,bb) = A(bb,aa)*W(bb)
		end do
	      end do	      
		
	      Npar(:,:,mcnt)=matmul(AtW,A) 
	      Mpar(:,mcnt)=matmul(AtW,slv)     	  
	      deallocate(AtW,A,slv,slW,W)   
	  
	  end do
    
	  ! --------------------------------------------------------------------                    
	  ! --------------------------------------------------------------------
	  ! -------------------------- COMPUTE VARIANCE COMPONENT ESTIMATE (VCE)
	  ! --------------------------------------------------------------------
	  ! -------------------------------------------------------------------- 
	   ! see Eicker thesis
	  allocate(Nr(tdsize), x(tdsize),Nli(tdsize,tdsize),Nl(tdsize,tdsize))
! 	  &
! 		  Nl2(tdsize,tdsize)
		  
	  allocate(var(missn),omg(missn),red(missn))
	  
	  var(1:missn)=missn ! initial weight: same for each mission
	  sumvar = 1
! 	  print*,'LON,LAT'
! 	  print*,lon/1000000.,lat/1000000.
! 	  print*,'nobs'
! 	  print*,cnt
	  
	  do kkk=1,4 ! iteration for 3-4 times for result convergence.
	  
	      ! ----------------------------------------------------------------
	      ! ----------------------------- Compute the total normal equations           
	      Nl(1:tdsize,1:tdsize)=0.
	      Nr(1:tdsize)=0.

	      do iii = 1,tdsize
           
            do jjj = 1,tdsize
                do ddd=1,missn
                Nl(iii,jjj) = Nl(iii,jjj) + Npar(iii,jjj,ddd)/(var(ddd)*sumvar)	
                end do
            end do      
		
            do ddd=1,missn
                Nr(iii) = Nr(iii) + Mpar(iii,ddd)/(var(ddd)*sumvar)
            end do	 
            
	      end do
		
  !           ! -------------------------------------- PRINT SESSION STARTS ----	            
  ! 	    
  ! 	    do ddd=1,missn
  ! 	      print*,'Npar',ddd
  ! 	      do gg=1,tdsize
  ! 		  write(*,*) ( Npar(gg,j,ddd), j=1,tdsize)
  ! 	      enddo
  ! 	    enddo
  ! 	    print*,'Ntot'
  ! 	    do gg=1,tdsize
  ! 	        write(*,*) ( Nl(gg,j), j=1,tdsize)
  ! 	    enddo 	 
  ! 	    ! ---------------------------------------- PRINT SESSION ENDS ------
		
		
	      ! --------------------------- end of total normal equation computation
	      ! --------------------------------------------------------------------
	      
	      ! --------------------------------------------------------------------
	      ! -------------------- COMPUTE INVERSE OF MATRIX Nl OF NORMAL EQUATION 
	      ! -------------------------- Use Moore-Penrose pseudo-inverse algoritm 
	      ! ----------------------------------------- from LAPACK SVD subroutine
	      ! http://www.netlib.org/lapack/lug/node53.html
	      ! see book michael schmidt gave you, about SS and formulae (write down reference)

	      lwork=5*(tdsize)
	      allocate(SS(tdsize),UU(tdsize,tdsize), &
			VT(tdsize,tdsize),worksvd(lwork))
			
	      call DGESVD('A','A',tdsize,tdsize,Nl,tdsize,SS,UU, &
			    tdsize,VT,tdsize,worksvd,lwork,infosvd)
	      deallocate(worksvd)
		
	      do si=1,tdsize
            if (SS(si) .lt. 0.001) then
                SS(si)=0.
            endif
	      end do
		
	      ! ------- Invert Singular values vector and create diagonal matrix
	      allocate(SDG(tdsize,tdsize))
	      SDG(1:tdsize,1:tdsize)=0.
      
	      do sx=1,tdsize
            if (SS(sx) .ne. 0) then
                SDG(sx,sx)=1/SS(sx)
            endif
	      end do
		  
	      ! ---------- Multiply UU*SDG*VT and get the pseudo-inverse of AtWA
	      allocate(UUSD(tdsize,tdsize))
	      UUSD = matmul(UU,SDG) 
	      Nli = matmul(UUSD,VT)      
	      deallocate(UUSD,SS,VT,UU,SDG)
	  
	      ! -------------------------------------------- END INVERSE COMPUTATION
	      ! --------------------------------------------------------------------
   
	      x=matmul(Nli,Nr)   
	      
	      ! save variance and error info for all missions, to compute the 
	      ! total error.
  	      sumvar = 0
!   	     
	      do ddd=1,missn  
            
            allocate(xAtWA(tdsize),ntr(tdsize,tdsize))
            
            do aa = 1,tdsize
                xAtWA(aa)= dot_product(x,Npar(1:tdsize,aa,ddd))     
            end do	    

            xAtWAx = dot_product(xAtWA,x)        	      
            xMpar  = dot_product(x,Mpar(:,ddd))
            
            ntr=matmul(Npar(:,:,ddd),Nli)
            ntrsum=0.
            do aa=1,tdsize
                ntrsum = ntrsum+ntr(aa,aa)
            enddo
            deallocate(xAtWA,ntr)

            red(ddd) = cnt(ddd)-ntrsum/var(ddd)
            omg(ddd) = xAtWAx+slWsl(ddd)-2*xMpar 
            ! variances
            var(ddd) = omg(ddd)/red(ddd)
            
            ! normalize the weights of the missions
			sumvar   = sumvar + 1/var(ddd)	
! 			print*,var(ddd),1/var(ddd)
! 			print*,sumvar
	      end do           
    
	  end do
		
	  deallocate(Npar,Mpar)  
	  deallocate(Nl)
	  deallocate(snull)
	  
	  ! --------------------------------------------------------------------
	  ! --------------------------------------------------------------------
	  ! ---------------------- END COMPUTE VARIANCE COMPONENT ESTIMATE (VCE)
	  ! --------------------------------------------------------------------        
	  ! --------------------------------------------------------------------  
	      
	  ! ----------------------------------------------------- compute errors        
	
	  errsum = sum(omg)
	  cntsum = sum(cnt)
	  sigma0 = sqrt(errsum/(cntsum-tdsize))    
	  
	  write(899,*) lon/1000000.,lat/1000000.,sigma0
! 
! 	  print*, 'check redundancy'
! 	  print*,sum(red)-sum(cnt)+tdsize
	  
	  allocate(sigmaX(tdsize))
	  do gg =1,tdsize                     
	      sigmaX(gg)=Nli(gg,gg)    
	  end do
	  
	  deallocate(Nli,whichm,slWsl,Nr)!,Nl2
	  deallocate(omg,red)
	  ! ------------------------------------------------- end compute errors

	  ! ------------------------------ now compute amplitude am and phase ph
	  
	  do aa=1,tdnum    
	  
	    jj = 4+aa*2  ! this is the starting index of the tidal constituents if
	                 ! considered drift, annual and semi-annual signals in the
	                 ! design matrix, at columns respectively 1,2,3,4,5 
	    
	    am  = sqrt(x(jj)*x(jj) + x(jj+1)*x(jj+1))  ! cm because SLA was in cm
	    ph  = atan2(x(jj+1),x(jj))/d2r                 ! deg
	      
	    sam = sqrt((sigmaX(jj)*2*x(jj))**2+ &          ! already in cm 
		           (sigmaX(jj+1)*2*x(jj+1))**2)/(2*am)  
			    
	    sph = ((x(jj)/am)**2)*abs(x(jj+1)/x(jj))* &
		    sqrt((sigmaX(jj)/x(jj))**2+ &
		    (sigmaX(jj+1)/x(jj+1))**2)/d2r   ! ATTENTION! divided by (180/pi), otherwise the result is in rads!) 

	    select case(darw(aa))
		    
	      case ('MM')                  
		write(308,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph
	      case ('MF')
		write(312,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph		
	      case ('K1')
		write(304,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph
	      case ('O1')
		write(303,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph	
	      case ('Q1')
		write(305,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph	
	      case ('M2')
		write(300,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph
	      case ('N2')
		write(302,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph
	      case ('2N2')
		write(311,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph	
	      case ('K2')
		write(307,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph
	      case ('P1')
		write(306,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph	
	      case ('S1')
		write(310,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph
	      case ('S2')
		write(301,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph
	      case ('M4')
		write(309,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph              
	      case ('SA')
		write(313,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph 
	      case ('SSA')
		write(314,*) lon/1000000.,lat/1000000.,x(jj),x(jj+1), &
		             sigmaX(jj),sigmaX(jj+1),am,ph,sam,sph 

	    end select
		  
	  end do	      

	  
! ------------------------------------------------------------------------------
! ----------------------------   PRINT AREA   ----------------------------------
! ------------------------------------------------------------------------------
! 
!       Print the weight of the missions, that is 1/var
        write(599,*) lon/1000000.,lat/1000000.,1/(var(1:missn)*sumvar)
!         
!       Print the intercept of the missions, that is the last elements
!       of the x vector (the number depends on the value of missn)
        write(499,*) lon/1000000.,lat/1000000.,x(2*tdnum+5+1:2*tdnum+5+missn)   
! 
!       Print the sea level trend computed for all missions (in cm/year)
        write(399,*) lon/1000000.,lat/1000000.,x(1)    
! 
!       Write Annual signal
		write(313,*) lon/1000000.,lat/1000000.,x(2),x(3), &
		             sigmaX(2),sigmaX(3)		  
! 		             
! 	    Write Semi-Annual signal
		write(314,*) lon/1000000.,lat/1000000.,x(4),x(5), &
		             sigmaX(4),sigmaX(5)

! 1000 format (8(f10.5))
! 2000 format (24(f10.5))
! 4000 format (2(f10.5))


!   !           longitude and latitude
!           print*,'LON,LAT'
! 	  print 4000,lon/1000000.,lat/1000000.
!   ! 	    number of observations for each mission
!           print*,'nobs'
! 	  print *,cnt
!   !           Std dev of normal equations for each mission	
!           print*,'miss_std'
! 	  print 2000, sqrt(var)
!   !           Drift, real part, imaginary part and offset of each mission
!           print*,'drift,re,im,miss_off'





! 	  print 2000,x(1:5),x(tdnum-6:tdnum), sigmaX(1:5),sigmaX(tdnum-6:tdnum)

	  
	  
	  
	  
	  
	  
	  !   !           Std dev of drift,re,im,offset for each mission
!           print*,'std of: drift,re,im,miss_off'
! 	  print *,sqrt(sigmaX)
!   !           Amplitude, phase
!           print*,'am,ph'
!           print *,am,ph
!   !          Std dev of am,ph
!           print*,'std of: am,ph'
!           print *,sam,sph
! 	  print*,'--------------------------------------------------------------------------------'
! !            Note that all is in cm!!!!!
          
! ------------------------------------------------------------------------------
! --------------------------   END PRINT AREA   --------------------------------
! ------------------------------------------------------------------------------

	  deallocate (x,sigmaX,var,cnt)
	    
        else
            deallocate(whichm)
        endif
        
	! ------------------------------------- end of computation for one node!


   
!    end do ! end loop that goes node by node



!    call cpu_time(finish)
!    print '("NODES = ",f6.3," seconds.")',finish-start
   close(16)
   close(888)
!    close(999)
   close(899)
   
   close(300)
   close(301)
   close(302)
   close(303)
   close(304)
   close(305)
   close(306)
   close(307)
   close(308)
   close(309)
   close(310)
   close(311)
   close(312)
   
   close(599)
   close(499)
   close(399)
   close(313)
   close(314)
!       
 ! -----------------------------------------------------------------------------    
 end program computeTIDES

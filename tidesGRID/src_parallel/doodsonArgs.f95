      subroutine doodsonArgs(mjd,tau,s,h,p,N,pp)
! !---------------------------------------------------------------------<------!
!                                          subroutine: doodsonArgs (from WOBO)   
! Last edit: 21 Nov 2018, changed N to -N because opposite of the elongation of the moon
! i.e. N = -omega, where omega = 234.9554 + (19341.36261*t  + ....)   
! 
!	
	IMPLICIT NONE
	double precision  ::   t,tau,s,h,p,N,pp,mjd,Tu0,gmst0,r,gmst
        double precision  ::   CIRCLE,rmod,pi,d2r
	
	pi     = 3.141592654d0
    d2r    = pi/180.d0
	CIRCLE = 2.*pi
	t    = (mjd-51544.5d0)/365250.d0 		 		
	Tu0   = (int(mjd)-51544.5d0)/36525.d0
	    
	! from: http://www.cv.nrao.edu/~rfisher/Ephemerides/times.html#ref9
	! compute mean sidereal time
	gmst0 = (24110.54841d0 + Tu0*(8640184.812866d0 + & 
		Tu0*(0.093104d0 - Tu0*6.2d-6)))/86400.d0
		
	! from: GPS Satellite Surveying, A.Leick,L.Rapoport, D. Tatarnikov
	! PAGE 148 for r and GMST definition
	! r is the ratio of the mean sidereal time to UT1
	r    = 1.002737909350795d0 + Tu0*5.9006d-11 - Tu0*Tu0*5.9d-15 
	rmod  = r*(mjd-int(mjd))
	
	! 15 are the degrees that must be multiplied to gmst to convert 
	! hours to degrees and 15*24(hours)=360(CIRCLE) because we need 
	! the hour angle
	gmst  = mod(CIRCLE*(gmst0+rmod),CIRCLE)  

	! form TAMURA (1987): A harmonic development of the tide-generating potential
	s   = mod((218.31664562999d0 + (4812678.81195750d0 + &
	      (-0.14663889d0+( 0.00185140d0-0.00015355d0*t)*t)*t)*t)*d2r,CIRCLE)
	h   = mod((280.46645016002d0 + ( 360007.69748806d0 + & 
	      ( 0.03032222d0+( 0.00002000d0-0.00006532d0*t)*t)*t)*t)*d2r,CIRCLE)
	p   = mod((83.35324311998d0 + (  40690.13635250d0 + &
	      (-1.03217222d0+(-0.01249168d0+0.00052655d0*t)*t)*t)*t)*d2r,CIRCLE)
	N   = -mod((234.95544499000d0 + (  19341.36261972d0 + &
	      (-0.20756111d0+(-0.00213942d0+0.00016501d0*t)*t)*t)*t)*d2r,CIRCLE)
	pp  = mod((282.93734098001d0 + (     17.19457667d0 + &
	      ( 0.04568889d0+(-0.00001776d0-0.00003323d0*t)*t)*t)*t)*d2r,CIRCLE)
	      
	tau = mod(gmst + pi - s,CIRCLE)
	
	if (tau .lt. 0d0) tau=(tau+CIRCLE)
	if (s .lt. 0d0)   s=(s+CIRCLE)            
	if (h .lt. 0d0)   h=(h+CIRCLE)
	if (p .lt. 0d0)   p=(p+CIRCLE)
	if (N .lt. 0d0)   N=(N+CIRCLE)
	if (pp .lt. 0d0)  pp=(pp+CIRCLE)
! 
      return
      end 
!      
! !---------------------------------------------------------------------<------!

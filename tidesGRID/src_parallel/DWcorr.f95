      subroutine DWcorr(dw,d1,d2,d3,d4,d5,d6,mjd,f,u)
! !---------------------------------------------------------------------<------!
!     subroutine: DWcorr (from WOBO,IHO,Pugh and Woodworth 2014)
!     Last edit: 21 Nov 2018, changed some patameters according to IHO
!
      IMPLICIT NONE
      double precision  ::   tau,s,h,p,N,pp,f,u,cosu,sinu,pihalf,u0,mjd,ucorr
      double precision  ::   gmst,pi,t,D2R,Tu0,r,rmod
      integer           ::   d1,d2,d3,d4,d5,d6
      character(LEN=3)  ::   dw
!
      pi=4.D0*ATAN(1.D0)
      D2R = pi/180
      pihalf = pi/2  ! constant for Doodson-Warburg phase lag correction
      call DoodsonArgs(mjd,tau,s,h,p,N,pp)
      ! compute the angular speed u0, from doodson numbers*fequencies
      u0 = d1*tau+d2*s+d3*h+d4*p+d5*N+d6*pp 
      
	  select case(trim(dw))
	  
	    case ('Mm','MM')
	    u = u0;
		f = 1.000 - 0.130*cos(N)
	    
	    case ('Mf','MF')
		u = u0 - D2R*(23.7*sin(N) - 2.7*sin(2*N) + 0.4*sin(3*N))
		f = 1.043 + 0.4141*cos(N)
	    
	    case ('K1')
		u = u0 -  D2R*(8.9*sin(N) - 0.68*sin(2*N) + 0.07*sin(3*N)) + pihalf
		f = 1.006 + 0.115*cos(N) - 0.009*cos(2*N)
	    
	    case ('O1','Q1')
		u = u0 + D2R*(10.8*sin(N) - 1.3*sin(2*N) + 0.2*sin(3*N))-pihalf
		f = 1.009 + 0.1871*cos(N) -0.015*cos(2*N)
	    
	    case ('J1')
		u = u0 - D2R*(12.9*sin(N) - 1.3*sin(2*N) + 0.2*sin(3*N))+pihalf
		f = 1.1029 + 0.1676*cos(N) - 0.017*cos(2*N) + 0.0016*cos(3*N)
	    
	    case ('M2','N2','MI2','NI2','2N2','MA2','MB2', 'MS4')
		u = u0 - 2.1*D2R*sin(N)
		f = 1.000 - 0.037*cos(N)
	    
	    case ('K2')
		u = u0 - D2R*(17.7*sin(N) - 0.7*sin(2*N))
		f = 1.024 + 0.286*cos(N) + 0.008*cos(2*N)
		
	    case ('L2')
		cosu = 1.  - 0.25*cos(2*p) - 0.11*cos(2*p-N) - & 
                       0.02*cos(2*(p-N)) - 0.04*cos(N)
		sinu =     - 0.25*sin(2*p) - 0.11*sin(2*p-N) - &
                       0.02*sin(2*(p-N)) - 0.04*cos(N)
		u = u0 +  atan2(sinu,cosu) + 2*pihalf
		f = sqrt(cosu*cosu + sinu*sinu)
		
	    case ('M1')
		cosu = 2*cos(p) + 0.4*cos(p-N)
		sinu =   sin(p) - 0.2*sin(p-N)
		u = u0 +  atan2(sinu,cosu) + pi
		f = sqrt(cosu*cosu + sinu*sinu)
		
	    case ('2Q1','RO1','SIG1','P1','PI1','FI1')
		u = u0 - pihalf
		f = 1.
	    
	    case ('CHI1','THE1','OO1')
		u = u0 + pihalf
		f = 1.
	    
	    case ('R2')
		u = u0 + pi
		f = 1.
        	    
	    case ('LM2')
	    u = u0 - 2.1*D2R*sin(N) + pi
	    f = 1.000 - 0.037*cos(N)

        case ('M4','MN4')
            u = u0 - 2*2.1*D2R*sin(N)
            f = (1.000 - 0.037*cos(N))*(1.000 - 0.037*cos(N))
        
        case ('M8')
            u = u0 - 4*2.1*D2R*sin(N)
            f = (1.000 - 0.037*cos(N))*(1.000 - 0.037*cos(N))* &
                (1.000 - 0.037*cos(N))    
        
        case ('S2','Sa','SA','SSA','Ssa','T2','MSF','MSQ','EP2',&
                'MTM','MKS','N4','M6','S4')
            u = u0
            f = 1.
            
        case ('M3')
            u = u0 - 3.21*D2R*sin(N)+pi
            f = (sqrt(1.000 - 0.037*cos(N)))**3
        
        case ('S1','S3')
            u = u0+pi
            f = 1.
        
	  end select
    return
    end    
!
! !---------------------------------------------------------------------<------!
!

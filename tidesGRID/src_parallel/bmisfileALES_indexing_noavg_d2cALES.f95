   program bmisfileALES_indexing
   !
   ! This routine reads in the information of nodes (node ID, lat,lon, path of 
   ! the node files) and writes files for each mission with data within a 
   ! certain cap-size (input variable=csize) 
   ! ---------------------------------------------------------------------------	  
   ! --------------------------------------------- PROCESS TOPEX-TYPE SATELLITES
   ! ---------------------------------------------------------------------------
      IMPLICIT NONE
      character(LEN=30)              :: pmva
      character(LEN=8)               :: nodenr
      character(LEN=120)             :: nodepath,iorb,eot,slf,dtu,itm,otd,mss
      character(LEN=120)             :: wtr,dtr,ion,ssb,tep,ivb,oer,otd17,otd15
      character(LEN=120)             :: nodefile,omk,dtc,outname,s32
      character*12,dimension(11)     :: ms   
      character*30,dimension(11)     :: prefix,prefnew,ndnew
      character(LEN=3)               :: cy,misref
      character(LEN=4)               :: ps
      character(LEN=120)             :: filename,pOut  
      character*12,dimension(11)     :: mnodes,misfold   
      integer*4                      :: id,lon,lat,isec,msec,mssh,mssh4
      integer*4                      :: glon,glat,hsat,ralt,coast
      integer*4                      :: recstart,recstop,dpt
      integer                        :: rnd,k,nf,onull,sz,szo,iosor,iosmk
      integer                        :: iosmis,r,q,mm,trec,sorb,dl1,dl2,dl3
      integer                        :: iosnode,tnull,lnull,recnum,iossl
      integer                        :: iosi,iosfes,ioseot,iost,cend,ndrec
      integer                        :: ioswt,iosdt,iosio,iosib,ioste,iosssb
      integer                        :: vrec,zz,ddd,oldpass,iosoe,ios,rJ3,recn
      integer                        :: rTP,rJ1,rJ2,rEN,rE2,rE1c,rE1g,rTPem,rJ1em,rJ2em
      integer                        :: iosv,iosvv,cobs,iosdc,iosh,countSWH,d2cMin
      integer*2                      :: wtrop,dtrop,ionos,errorb,otide,ltide
      integer*2                      :: otide17,ltide17,otide15,ltide15
      integer*4                      :: pass,startrr,endrr,cc,j
      integer*2                      :: etide,ptide,invb,emb,slaint
      integer*1                      :: oflags,gflags,iflags,ws,msk
      integer*2                      :: sla31,st,sw,ssw,sig0
      real                           :: start1,finish1,start2,finish2,start3,finish3
      real                           :: start4,finish4,start5,finish5,start6,finish6
      real                           :: start7,finish7      
      logical                        :: fe,iex,tex,lex,ionex,msex,slex,exfes,dcex
      double precision               :: jd85,mjd85,tm,cradsq,dx,dy,dist,sla
      double precision               :: dum,slh,dum3,erad,tmav,slaav,wgav
      double precision               :: avTP,avJ1,avJ2,avEN,avE2,avE1c,avE1g
      double precision               :: stTP,stJ1,stJ2,stEN,stE2,stE1c,stE1g
      double precision               :: stSWH,avSWH,sumSWH,sumSWHsq
      double precision               :: minSWH,maxSWH,slawg,sumwg,maxWG,maxTM
      double precision               :: pi,d2r,sigma,wg,ar,csize,ttt,dlon,dlat 
      double precision,dimension(11) :: avg,slasum,slasqr,stdsla,sumsl,sumslsq

      read(*,*) csize
!       print*, csize
      read(*,'(a)') nodepath 
!       print*,nodepath
      read(*,*) ndrec
!       print*,ndrec
      read(*,*) mm
!       print*,mm
      read(*,'(a)') filename
!       print*,filename
      read(*,'(a)') pOut
!       print*,pOut
!        stop
       
      pmva='/DGFI8/A/MVA/'
    
      cy='ccc'

      ! 1Hz-frequency data
      ms(1)='topex'
      ms(2)='jason1'
      ms(3)='jason2'
      ms(4)='envisat_v3'
      ms(5)='ers2'
      ms(6)='ers1c'
      ms(7)='ers1g'
      ms(8)='topex_em'
      ms(9)='jason1_em'
      ms(10)='jason2_em'
      ms(11)='jason3'    
      
      prefix(1)='d2p_tp_'
      prefix(2)='d2p_j1_'
      prefix(3)='d2p_j2_'
      prefix(4)='d2p_en_'
      prefix(5)='d2p_e2_'
      prefix(6)='d2p_e1c_'
      prefix(7)='d2p_e1g_'
      prefix(8)='d2p_tpem_'
      prefix(9)='d2p_j1em_'
      prefix(10)='d2p_j2em_'
      prefix(11)='d2p_j3_'
      
      misfold(1)='TPnodes'
      misfold(2)='J1nodes'
      misfold(3)='J2nodes'
      misfold(4)='EN_V3nodes'
      misfold(5)='E2nodes'
      misfold(6)='E1Cnodes'
      misfold(7)='E1Gnodes'
      misfold(8)='TPEMnodes'
      misfold(9)='J1EMnodes'
      misfold(10)='J2EMnodes'
      misfold(11)='J3nodes'
      
      mnodes(1)='TP.bin'
      mnodes(2)='J1.bin'
      mnodes(3)='J2.bin'
      mnodes(4)='EN.bin'
      mnodes(5)='E2.bin'
      mnodes(6)='E1c.bin'
      mnodes(7)='E1g.bin'
      mnodes(8)='TPEM.bin'
      mnodes(9)='J1EM.bin'
      mnodes(10)='J2EM.bin'
      mnodes(11)='J3.bin' 
      
      ! ------------------- define PI, deg-to-rad conversion, and time from 1985
      pi    = 3.141592654d0
      d2r   = pi/180.
      erad  = 6371.
      jd85  = 2446066.5d0
      mjd85 = jd85-2400000.5d0     
      
      cradsq=csize*csize/4   ! this must be the radius (half csize)
      ttt=csize*0.4d0/2
      sigma=log(2.d0)/(ttt*ttt)	
      
      ! ------------------------------------------------------------------------	  
      ! --------------------------------------- START PROCESS MISSION-BY-MISSION
      ! ------------------------------------------------------------------------
      recn=0   ! initialize count of record for each node for each mission
     
      write(misref(1:3),'(I3.3)') mm
      open (800+mm,file=trim(pOut)//'/cnt_'//trim(misref)) 

    
        open (16,file=filename,access='direct',recl=16, &
        form='unformatted',status='OLD',iostat=ios)
        read (16,rec=ndrec) id,lon,lat,dpt 
        
       
        vrec=1
        
        ! create the file where to save the data for each mission for each node
        write(nodenr(1:8),'(I8.8)') id
        outname = trim(prefix(mm))//nodenr(1:8)
        nf=80+mm
        open (nf,file=outname,access='direct',recl=24,&
            form='unformatted',status='REPLACE',iostat=iosmis)	
        
        ! read the file of the mission where all the passes for each node are stored
        write(nodenr(1:8),'(I8.8)') id
        nodefile=trim(nodepath)//'/'//trim(misfold(mm))//'/'// & 
                nodenr//'_'//trim(mnodes(mm))
!         print*,nodefile
        
        INQUIRE(file=nodefile,exist=fe,SIZE=sz)
        rnd=sz/16 
        
        if (fe) then ! if there are tracks for this node (if file exists)  
	  
          open (111,file=nodefile,access='direct',recl=16, &
          form='unformatted',status='OLD',iostat=iosnode)  

            ! change minimum distance to coast according to mission (ALES until 3 km to coast)
            if (mm .eq. 2 .or. mm .eq. 3 .or. mm .eq. 11 .or. &
                mm .eq. 10 .or. mm .eq. 9 .or. mm .eq. 4) then
                d2cMin=3000
            else
                d2cMin=6000
            endif
          
          do k=1,rnd  ! go through all passes of interest for the node
          

            read(111,rec=k) cc,pass,startrr,endrr
            
            ! add +1 to the record number because the indexing was made with Python 
            ! and the counting starts from 0 (not accepted in fortran as record number)             
            startrr=startrr+1
            endrr=endrr+1     
    
            write(cy(1:3),'(I3.3)') cc
                
            if (mm .le. 3 .or. mm .ge. 8) then
                ps='ppp'
                write(ps(1:3),'(I3.3)') pass
            elseif (mm .gt. 3 .and. mm .le. 7) then
                ps='pppp'
                write(ps(1:4),'(I4.4)') pass             
            endif   

            ! ----------------------------------- open binary files from MVA		
            ! ------------------------------ check files exist and not empty
            ! --------------------------------------------------- orbit file
            if (mm .eq. 2 .or. mm .eq. 9 .or. mm .eq. 10 .or. & 
                mm .eq. 11 .or. mm .eq. 4) then
                iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
                        '/'//cy//'_'//trim(ps)//'orbit.00'
            elseif (mm .eq. 3) then
                iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
                        '/'//cy//'_'//trim(ps)//'orbit.30'
            elseif (mm .eq. 1 .or. mm .eq. 5 .or. mm .eq. 6 .or. &
                    mm .eq. 7 .or. mm .eq. 8) then
                iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
                        '/'//cy//'_'//trim(ps)//'orbit.24'
            endif
            !        Old envisat     elseif (mm .eq. 4) then
!                 iorb=trim(pmva)//trim(ms(mm))//'/'//cy//&
!                         '/'//cy//'_'//trim(ps)//'orbit.06' 
            
            INQUIRE(file=iorb,exist=iex,size=sorb)
                
            ! --------------------------------------------------  instr file
            itm = trim(pmva)//trim(ms(mm))//'/'//cy// &
                '/'//cy//'_'//trim(ps)//'instr.00'	        
            INQUIRE(file=itm,exist=tex)
                                                
            ! ---------------------------------------- Distance to coast .00
            dtc = trim(pmva)//trim(ms(mm))//'/'//cy// &
            '/'//cy//'_'//trim(ps)//'distcoast.00'
            INQUIRE(file=dtc,exist=dcex)
            
            ! ------------------------------------------------------ SLA .34
            s32 = trim(pmva)//trim(ms(mm))//'/'//cy// &
            '/'//cy//'_'//trim(ps)//'slafg.34'
            INQUIRE(file=s32,exist=slex)

            ! -------------------------------------- If files exist, open...
            if (iex .and. tex .and. slex .and. dcex & 
                    .and. sorb .gt. 100) then
                
                
                open (30,file=iorb,access='direct',recl=13, &
                form='unformatted',status='OLD',iostat=iosor)   
                
                open (40, file=itm,access='direct',recl=22, &
                    form='unformatted',status='OLD',iostat=iost)
                    
                open (41, file=otd17,access='direct',recl=4, &
                    form='unformatted',status='OLD',iostat=iosfes)
                    
                open (42, file=otd15,access='direct',recl=4, &
                    form='unformatted',status='OLD',iostat=ioseot)
            
                open (78, file=dtc,access='direct',recl=4, &
                    form='unformatted',status='OLD',iostat=iosdc)	

                open (98, file=s32,access='direct',recl=3, &
                    form='unformatted',status='OLD',iostat=iossl)
                    
                ! ------- ... and read the records indicated in the nodefile
                trec=sorb/13
               
                do j=startrr,endrr    !1,trec
        
!                     print*,j
                    ! ----- read file and check what are the data to account 

                    read(30,rec=j) glon,glat,hsat,oflags
                    
                    ! ------------------------------------- compute distance 
                    ! we basically apply the Pythagorean theorem
                    ! on a sphere projected on a plane (accounting for the 
                    ! different distance between meridians according to latitude)
                    ! account for the extreme longitudes: take the smallest
                    ! distance between the node and the track point in longitude. 
                    if (glat .le. lat+4000000 .and. glat .ge. lat-4000000) then
                        
                        dl1 = abs(lon - glon)
                        dl2 = abs(lon - glon + 360000000)
                        dl3 = abs(lon - glon - 360000000)
                    
                        dlon = min(dl1,dl2,dl3)*d2r/1000000.
                        dlat = abs(lat - glat)*d2r/1000000.     
                        dx   = dlon*erad*cos(glat*d2r/1000000.)
                        dy   = dlat*erad
                        dist = dx*dx+dy*dy     ! N.B. already square value
                    
                        
                        read(40,rec=j) isec,msec,ralt,st,sw,ssw,sig0,ws,iflags
                        read(98,rec=j) slaint,gflags                            
                        read(78,rec=j) coast
                            
!                             call cpu_time(start5)   ! measure time spent to compute SLA
                        
                        sla = dble(slaint)

                        ! ----- if SLA are reasonable and within cap size, go on
                        
                        if (sla .ge. -2500 .and. sla .le. 2500 .and. & 
                            dist .le. cradsq .and. gflags .eq. 0 .and. &
                            oflags .le. 16 .and. iflags .le. 4 .and. &
                            coast .gt. d2cMin) then

                            wg   = exp(-sigma*dist)
                                                        
                            ! ----------------------------- compute time in JDAY
                            tm=(dble(isec)+(dble(msec)/1000000.))/ &
                                86400.d0+ mjd85	
                            
                            ! ATTENTION!! SLA is saved in cm!!! 
                            write(nf,rec=vrec) tm,sla/10,wg    
!                             print*,mm,glon/1000000.,glat/1000000.,tm,sla/10,wg  
                            vrec=vrec+1 
                            recn=recn+1
                
                         endif ! end condition to accept SLA   
                    endif     ! end latitude bound condition
                end do    ! finish one track (i.e. one line of nodefile) 


                close(30)    
                close(40)
                close(98)
                    
                    
                endif ! end condition if track file exists  

          end do         ! end loop through pass
          
        endif            ! end condition if pass file exists          

        close(nf)

      
      ! ------------------------------------------------------------------------	  
      ! ------------------------------------------------------------ END PROCESS
      ! ------------------------------------------------------------------------   
      
      ! ----------------- SAVE COUNT OF OBSERVATION FOR EVERY MISSION AT EACH NODE 
      write(800+mm,*) lon/1000000.,lat/1000000.,recn    
      close(800+mm)


   end program
   

#!/bin/bash
#
# This scripts add the amplitude and phase of the residuals to the ones of FES14.
# Now we are operating along track, therefore we will use grdtrack to interpolate 
# the real and imaginary part of FES14 on the track.
# Then, with AWK we compute the new amplitude and phase.
# Information on track, region and tidal constituent is read from 'mainAT'
################################################################################
read tide
read tidesmall
read outfile
read tidefile
read reg
################################################################################
#
# 1. Get the real and imaginary part of FES14
#
#    1.a prepare FES14 grids 
pathFES=/DGFI93/home/piccioni/Dokumente/FES2014/libfes-2.7.4/data/fes2014/ocean_tide
sp=3.75m
#        1.a.a amplitude
cp $pathFES/$tidesmall.nc .
grdedit $tidesmall.nc?amplitude -Rd -S
grdsample $tidesmall.nc -GsmallFESam_$tide.grd -R$reg -I$sp
#        1.a.b phase
cp $pathFES/$tidesmall.nc . 
grdedit $tidesmall.nc?phase -Rd -S
grdsample $tidesmall.nc -GsmallFESph_$tide.grd -R$reg -I$sp
rm $tidesmall.nc
#
#    1.b prepare real and imaginary part of FES14
grdmath smallFESph_$tide.grd COSD smallFESam_$tide.grd MUL = reFES.grd
# grdfilter reFES.grd -GreFESf.grd -D4 -Fg15m
grdmath smallFESph_$tide.grd SIND smallFESam_$tide.grd MUL = imFES.grd
# grdfilter imFES.grd -GimFESf.grd -D4 -Fg15m
#
################################################################################
#
# 2. Prepare real and imaginary parts of residuals 
# IMPORTANT: CHECK THAT THE NUMBER OF NODES IS THE SAME
#
#    3.a real part
awk '$1<=180{print $1,$2,$3}' $tidefile > reRES1
awk '$1>180{print $1-360,$2,$3}' $tidefile > reRES2
cat reRES1 reRES2 > reRES
#
#    3.b imaginary part
awk '$1<=180{print $1,$2,$4}' $tidefile > imRES1 
awk '$1>180{print $1-360,$2,$4}' $tidefile > imRES2
cat imRES1 imRES2 > imRES
#
#    3.c get the coordinates to interpolate FES14
awk '{print $1,$2}' imRES > coords
#
################################################################################
#
# 3. Interpolate FES14 re and im parts on track
#
grdtrack coords -GreFES.grd -nl+t0.1 -N > reFES 
grdtrack coords -GimFES.grd -nl+t0.1 -N > imFES
#
# interpolate fes to compare with results
grdtrack coords -GsmallFESam_$tide.grd -nl+t0.1 -N > amFES
grdtrack coords -GsmallFESph_$tide.grd -nl+t0.1 -N > phFES
pr -m -t -s\  amFES phFES | awk '{print $1,$2,$3,$6}' > fes_$tide.txt
# 
#
################################################################################
#
# 4. compute new amplitude and phase 
#    (one reference for this is page 104 of Schureman 1958, formulae 444, 445)
#
#    4.a sum real and imaginary parts:
pr -m -t -s\  reFES reRES | awk '{print $1,$2,$3+$6}' > reTOT
pr -m -t -s\  imFES imRES | awk '{print $1,$2,$3+$6}' > imTOT
#
#    4.b compute amplitude
awk '{print $1,$2,$3^2}' reTOT > reSQ
awk '{print $1,$2,$3^2}' imTOT > imSQ
pr -m -t -s\  reSQ imSQ | awk '{print $1,$2,sqrt($3+$6)}' > outam
#
#    4.c compute phase
pr -m -t -s\  reTOT imTOT | awk '{print $1,$2,atan2($6,$3)*180/3.14159265359}' > outph
#
#    4.d merge to the same file
pr -m -t -s\  outam outph | awk '{print $1,$2,$3,$6}' > $outfile


################################################################################
#
# *. INTERPOLATE also EOT11a to compare it with the other models
# EOT11a
cp /DGFI93/home/piccioni/Dokumente/EOT11a/data/$tide.eot11a.nc . 
grdedit $tide.eot11a.nc?re -Rd -S
grdsample $tide.eot11a.nc -GreEOT.grd -R$reg -I$sp
rm $tide.eot11a.nc
cp /DGFI93/home/piccioni/Dokumente/EOT11a/data/$tide.eot11a.nc . 
grdedit $tide.eot11a.nc?im -Rd -S
grdsample $tide.eot11a.nc -GimEOT.grd -R$reg -I$sp
rm $tide.eot11a.nc

# amplitude
grdmath reEOT.grd SQR imEOT.grd SQR ADD SQRT = eot11a.grd                          
# phase
grdmath imEOT.grd reEOT.grd ATAN2 180 MUL PI DIV = eot11p.grd                          
grdtrack coords -Geot11p.grd -nl -N > phEOT.pos
grdtrack coords -Geot11a.grd -nl -N > amEOT.pos
pr -m -t -s\  amEOT.pos phEOT.pos | awk '{print $1,$2,$3,$6}' > eot_$tide.txt
rm eot11a.grd
rm eot11p.grd
rm amEOT.pos
rm phEOT.pos
################################################################################



rm re*
rm im*
rm *.grd 
rm outph
rm outam

#!/bin/bash
#
# This scripts add the amplitude and phase of the residuals to the ones of FES14.
# Now we are operating along track, therefore we will use grdtrack to interpolate 
# the real and imaginary part of FES14 on the track.
# Then, with AWK we compute the new amplitude and phase.
# Information on track, region and tidal constituent is read from 'mainAT'
################################################################################
read tide
read tidesmall
read tidefile
read reg
read sp
read dst
################################################################################
#
# 1. Get the real and imaginary part of FES14
#
#    1.a prepare FES14 grids (change longitude domain with grdedit (NORTH SEA COORDINATES))
#        N.B. in the end we want to compare the grids with their original resolution, therefore
#             for FES we have an output used for comparison, and an output to be added to the
#             residuals (am_FES14.grd and ph_FES14.grd) because we need to add grids with same resolution.
pathFES=/DGFI93/home/piccioni/Dokumente/FES2014/libfes-2.7.4/data/fes2014/ocean_tide_extrapolated

#        1.a.a amplitude
grdsample $pathFES/$tidesmall.nc?amplitude -Gam_FES14.grd -Rg$reg -I$sp

#
#        1.a.b phase
grdsample $pathFES/$tidesmall.nc?phase -Gph_FES14.grd -Rg$reg -I$sp


#    1.b prepare real and imaginary part of FES14
grdmath ph_FES14.grd COSD am_FES14.grd MUL = reFES.grd
grdmath ph_FES14.grd SIND am_FES14.grd MUL = imFES.grd
#
# # #
# ################################################################################
# #
# # 2. make grids from residuals
# #
#    2.a real part 
awk '{print $1,$2,$3}' $tidefile > re.pos
blockmean re.pos -Rg$reg -I$sp > reRESm.pos
surface reRESm.pos -GreRES.grd -Rg$reg -I$sp
# grdfilter reRES.grd -GreRESf.grd -D4 -Fg60m
#
#    2.b imaginary part
awk '{print $1,$2,$4}' $tidefile > im.pos
blockmean im.pos -Rg$reg -I$sp > imRESm.pos
surface imRESm.pos -GimRES.grd -Rg$reg -I$sp
# grdfilter imRES.grd -GimRESf.grd -D4 -Fg60m
#
rm *.pos
#
################################################################################
#
# 3. compute total amplitude and phase WITH FES2014 
#
#    !!!!!! N.B. if we want to use EOT11a, please comment this 
#    !!!!!! section and decomment point 5. 
#
#    (one reference for the following formulae is 
#     p. 104 of Schureman 1958, equations 444, 445)
#
#    3.a sum real and imaginary parts:
grdmath reFES.grd reRES.grd ADD = reTOT.grd
cp reTOT.grd $dst/re_$tide\_EOT18.grd
# 
grdmath imFES.grd imRES.grd ADD = imTOT.grd
cp imTOT.grd $dst/im_$tide\_EOT18.grd
#
#    3.b get amplitude and phase
grdmath reTOT.grd SQR imTOT.grd SQR ADD SQRT = $dst/am_$tide\_EOT18.grd
# amS
grdmath imTOT.grd reTOT.grd ATAN2 PI DIV 180 MUL = $dst/ph_$tide\_EOT18.grd
# phS
#
# filter=$(awk 'BEGIN{print 2*'$sp'}')
# grdfilter amS -G$dst/am_$tide\_EOT18.grd -D4 -Fg$filter
# grdfilter phS -G$dst/ph_$tide\_EOT18.grd -D4 -Fg$filter
# grdfilter reTOT.grd -G$dst/re_$tide\_EOT18.grd -D4 -Fg$filter
# grdfilter imTOT.grd -G$dst/im_$tide\_EOT18.grd -D4 -Fg$filter

# rm amS
# rm phS
rm imTOT.grd
rm reTOT.grd
rm am_FES14.grd
rm ph_FES14.grd
rm imRES.grd
rm reRES.grd
rm imFES.grd
rm reFES.grd
# #
################################################################################
################################################################################
################################################################################
################################################################################
#      ALTERNATIVE WAY TO BE TESTED FOR IRREGULAR GRIDS OF RESIDUALS
################################################################################
################################################################################
################################################################################
################################################################################
# # # #
# # # # 2. im,re parts from residuals
# # # #
# # #    2.a real part 
# # echo $tidefile
# # awk '$1>180{print $1-360,$2,$3}' $tidefile > re1
# # awk '$1<=180{print $1,$2,$3}' $tidefile > re2 
# # cat re1 re2 > reRES.pos
# # #    2.b imaginary part
# # awk '$1>180{print $1-360,$2,$4}' $tidefile > im1
# # awk '$1<=180{print $1,$2,$4}' $tidefile > im2 
# # cat im1 im2 > imRES.pos
# # #
# # # rm *.pos
# # #
# # ################################################################################
# # #
# # # 3. compute total amplitude and phase WITH FES2014 
# # 
# # #    prepare real and imaginary part of FES14 (use original resolution)
# # grdmath $dst/ph_$tide\_FES14.grd COSD $dst/am_$tide\_FES14.grd MUL = reFES.grd
# # grdmath $dst/ph_$tide\_FES14.grd SIND $dst/am_$tide\_FES14.grd MUL = imFES.grd
# # 
# # #    interpolate FES on grid of residuals (grdtrack = coluns 1-3: reRES.pos + column 4: reFES.grd) 
# # grdtrack reRES.pos -GreFES.grd > reFES.pos  
# # grdtrack imRES.pos -GimFES.grd > imFES.pos
# # 
# # # sum real and imaginary parts:
# # awk '{print $1,$2,$3+$4}' reFES.pos > reTOT.pos
# # awk '{print $1,$2,$3+$4}' imFES.pos > imTOT.pos
# # 
# # #
# # #    3.b get amplitude and phase
# # pr -m -t -s\  reTOT.pos imTOT.pos | awk '{print $1,$2,$3,$6}' > reim.pos
# # awk '{print $1,$2,sqrt($3^2+$4^2)}' reim.pos > am.pos
# # awk '{print $1,$2,atan2($4,$3)*180/3.14159265}' reim.pos > ph.pos
# # 
# # # 3.c interpolate to grid with spacing sp
# # blockmean am.pos -Rg$reg -I$sp > amM.pos
# # blockmean ph.pos -Rg$reg -I$sp > phM.pos
# # surface amM.pos -GamS -Rg$reg -I$sp
# # surface phM.pos -GphS -Rg$reg -I$sp
# # 
# # # $dst/am_$tide\_EOT18.grd  $dst/ph_$tide\_EOT18.grd
# # 
# # filter=$(awk 'BEGIN{print 2*'$sp'}')
# # grdfilter amS -G$dst/am_$tide\_EOT18.grd -D4 -Fg$filter
# # grdfilter phS -G$dst/ph_$tide\_EOT18.grd -D4 -Fg$filter

################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
# # 
# # # # #
# # # 4. INTERPOLATE also EOT11a to compare it with the other models
# # # 
# # #    4.a.a Real part   
# # grdsample /DGFI93/home/piccioni/Dokumente/EOT11a/data/$tide.eot11a.nc?re -GreEOT.grd -Rg$reg -I$sp
# # # 
# # #    4.a.b Imaginary part  
# # grdsample /DGFI93/home/piccioni/Dokumente/EOT11a/data/$tide.eot11a.nc?im -GimEOT.grd -Rg$reg -I$sp
# # # 
# # #    4.b Compute amplitude and phase
# # # grdmath reEOT.grd SQR imEOT.grd SQR ADD SQRT = $dst/am_$tide\_EOT11.grd                         
# # # grdmath imEOT.grd reEOT.grd ATAN2 180 MUL PI DIV = $dst/ph_$tide\_EOT11.grd                     
# # # # 
# # # rm re*.grd
# # # rm im*.grd
# # # # ################################################################################
# # # # #
# # # # # 5. Note that if we want to use EOT11a instead of FES2014 as basic model,
# # # # #    we can comment step 3 and use the following step instead:
# # 
# # #    5.a sum real and imaginary parts:
# # grdmath reEOT.grd reRES.grd ADD = reTOT.grd
# # grdmath imEOT.grd imRES.grd ADD = imTOT.grd
# # 
# # #    5.b get amplitude and phase
# # grdmath reTOT.grd SQR imTOT.grd SQR ADD SQRT = amS
# # grdmath imTOT.grd reTOT.grd ATAN2 PI DIV 180 MUL = phS
# # #
# # filter=$(awk 'BEGIN{print 2*'$sp'}')
# # grdfilter amS -G$dst/am_$tide\_EOT18.grd -D4 -Fg$filter
# # grdfilter phS -G$dst/ph_$tide\_EOT18.grd -D4 -Fg$filter
# # 
# # rm amS
# # rm phS
# # rm imTOT.grd
# # rm reTOT.grd
# # rm imRES.grd
# # rm reRES.grd

# # #
# # ################################################################################
# # #
# # # 4*. INTERPOLATE also TPXO8 to compare it with the other models
# # #
# # # Amplitude, already in cm 
# # grdsample /DGFI93/home/piccioni/Dokumente/TPXO8/amph/$tidesmall\_a.nc -G$dst/am_$tide\_TPXO8.grd -Rg$reg
# # # Phase, already in degrees
# # grdsample /DGFI93/home/piccioni/Dokumente/TPXO8/amph/$tidesmall\_p.nc -G$dst/ph_$tide\_TPXO8.grd -Rg$reg
# # #
# # ################################################################################
# # #
# # # 4*. INTERPOLATE also DTU10 to compare it with the other models
# # # N.B. already as netCDF files of amplitude and phase in folder: 
# # #  /DGFI93/home/piccioni/Dokumente/DTU10/nc/)
# # # Amplitude, already in cm
# # grdsample /DGFI93/home/piccioni/Dokumente/DTU10/nc/$tide\_a.nc -G$dst/am_$tide\_DTU10.grd -Rg$reg
# # # Phase, already in degrees
# # grdsample /DGFI93/home/piccioni/Dokumente/DTU10/nc/$tide\_p.nc -G$dst/ph_$tide\_DTU10.grd -Rg$reg
# # #
# # ################################################################################
# # #
# # # 4*. INTERPOLATE also GOT4.8 to compare it with the other models
# # # N.B. already as netCDF files of amplitude and phase in folder: 
# # #  /DGFI93/home/piccioni/Dokumente/GOT4.8)
# # #
# # # Amplitude, already in cm
# # grdsample /DGFI93/home/piccioni/Dokumente/GOT4.8/amph/$tidesmall\_a.nc -G$dst/am_$tide\_GOT48.grd -Rg$reg
# # # Phase, already in degrees
# # grdsample /DGFI93/home/piccioni/Dokumente/GOT4.8/amph/$tidesmall\_p.nc -G$dst/ph_$tide\_GOT48.grd -Rg$reg
# # #
# # ################################################################################
# # #
# # # 4*. INTERPOLATE also FES2012 to compare it with the other models
# # pathF12=/DGFI8/D/tides/FES2012
# # #
# # #        1.a.a amplitude
# # grdsample $pathF12/$tide.FES2012.elev.nc?Ha -G$dst/am_$tide\_FES12.grd -Rg$reg
# # #
# # #        1.a.b phase
# # grdsample $pathF12/$tide.FES2012.elev.nc?Hg -G$dst/ph_$tide\_FES12.grd -Rg$reg
# # # echo 'done'
# # 
# # ################################################################################
# # #
# # # 4*. INTERPOLATE also FES2004 to compare it with the other models
# # pathF04=/DGFI8/D/tides/FES2004/FES2004.new/FES2004/fes2004b/GRD/
# # #
# # #        1.a.a amplitude
# # grdsample $pathF04/$tide\_fes2004.ampl.grd -G$dst/am_$tide\_FES04.grd -Rg$reg
# # #
# # #        1.a.b phase
# # grdsample $pathF04/$tide\_fes2004.phas.grd -G$dst/ph_$tide\_FES04.grd -Rg$reg
# # # echo 'done'
# # 

I have created this GIT repository to store the tidesGRID data so that we will always have the original version. 

This will allow us to easily maintain the version of tidesGRID and allow us to do quality checks of the scripts. 

I will create a "working" branch were we are building and testing changes to the code. Once the code works and we are happy with it, we can then push to the Master branch.

Repository is private and can be accessed by my local computer.

-------------------------------------------------------------------------------
Michael Hart-Davis, 26.11.2019
